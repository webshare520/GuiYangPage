﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GuiYangPageBll;
using System.Web.SessionState;
using GuiYangPageModel;

namespace GuiYangPage.Command
{
    /// <summary>
    /// GetLogin 的摘要说明
    /// </summary>
    public class GetLogin : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //先校验验证码
            string userCode = context.Request["code"];
            string sessionVcode = context.Session["Vcode"] as string;
            //验证一次 立即清空
            context.Session["Vcode"] = null;
            if (string.IsNullOrEmpty(sessionVcode) || userCode != sessionVcode)
            {
                context.Response.Write("errorCode");
                return;
            }
            //采集数据
            else if (context.Request["username"].ToString() != "" && context.Request["password"].ToString().Trim() != "")
            {
                string name = context.Request["username"].ToString().Trim();
                string pwd = context.Request["password"].ToString().Trim();
                CpyAdminBll bll = new CpyAdminBll();
                CpyAdmin admin = new CpyAdmin();
               
               
                bool ok = bll.GetLogin(name, pwd);

                if (ok)
                {
                    context.Response.Write("1");
                    context.Session["loginadmin"] = ok;
                }
                else
                {
                    context.Response.Write("0");
                }
            }
            else {
                context.Response.Write("empty");
            }
          
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
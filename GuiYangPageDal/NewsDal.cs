﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using GuiYangPageModel;
using Common;
using System.Data.OleDb;
namespace GuiYangPageDal
{
    public class NewsDal
    {



        DatabaseSql db;
        //构造函数
        public NewsDal()
        {
            db = new DatabaseSql();

        }
        //新闻详情
        public News NewsModel(int id)
        {
            string sql = "select * from news where newid=@id";
            SqlParameter[] pms = { 
                                 new SqlParameter("@id",SqlDbType.Int){Value=id}
                                 };
            DataTable dt = db.ExecuteDataTable(sql, CommandType.Text, pms);
            News newInfo = null;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                newInfo = new News();
                newInfo.Title = dt.Rows[i]["Title"] != DBNull.Value ? dt.Rows[i]["Title"] as string : string.Empty;
                newInfo.Picture = dt.Rows[i]["Picture"] != DBNull.Value ? dt.Rows[i]["Picture"] as string : string.Empty;
                newInfo.Content = dt.Rows[i]["Content"] != DBNull.Value ? dt.Rows[i]["Content"] as string : string.Empty;
                newInfo.Date = DateTime.Parse(dt.Rows[i]["Date"].ToString());
            }
            return newInfo;
        }

        ///  <summary>
        ///  获取新闻记录总数
        ///</summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM News ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = db.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        /// <param name="strWhere">要查询的条件（）</param>
        /// <param name="orderby"></param>
        /// <param name="startIndex">当前页</param>
        /// <param name="endIndex">当前要显示的条数</param>
        /// <returns></returns>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.NewId asc");
            }
            strSql.Append(")AS Row, T.*  from News T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return db.Query(strSql.ToString());
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public News DataRowToModel(DataRow row)
        {
            News model = new News();
            if (row != null)
            {
                if (row["NewId"] != null && row["NewId"].ToString() != "")
                {
                    model.NewId = int.Parse(row["NewId"].ToString());
                }
                if (row["Title"] != null)
                {
                    model.Title = row["Title"].ToString();
                }
                if (row["Content"] != null)
                {
                    model.Content = row["Content"].ToString();
                }
                if (row["Date"] != null && row["Date"].ToString() != "")
                {
                    model.Date = DateTime.Parse(row["Date"].ToString());
                }

                if (row["Picture"] != null)
                {
                    model.Picture = row["Picture"].ToString();
                }
            }
            return model;
        }



        #region ==================关于 后台用easyui 得到新闻消息的方法=============================


        /// <summary>
        ///   定义一个方法。得到所有新闻的信息
        /// </summary>
        /// <returns></returns>
        //public DataSet getNews()
        //{
        //    string sql = "select * from News";
        //    DataSet ds = db.GetDataSet(sql);

        //    return ds;
        //}


        //分页得到新闻
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNo">第几页</param>
        /// <param name="pageSize">一页多少条</param>
        /// <param name="title">新闻标题</param>
        /// <returns>要返回的值</returns>
        public DataSet getNewsByPage(int pageNo, int pageSize, string title)
        {
            // 
            string sql = "select top " + pageSize + "  * from news where NewId not in(select top  " + (pageNo - 1) * pageSize + " NewId from  news where title like '%" + title + "%' order by Date) and title like '%" + title + "%' order by Date ";
            DataSet ds = db.GetDataSet(sql);
            return ds;
        }
        //得到新闻的数量
        public int getNewsCount(string title)
        {
            string sql = "select * from news where title like '%" + title + "%'";
            DataSet ds = db.GetDataSet(sql);
            return ds.Tables[0].Rows.Count;
        }
        //根据资讯ID删除资讯内容
        public int deleteNews(string[] ids)
        {
            string sql = "";
            for (int i = 0; i < ids.Length; i++)
            {
                if (i == 0)
                    sql = "delete news where NewId= " + ids[i];
                else
                    sql = sql + " or NewId=" + ids[i];
            }
            int newscount = db.ExecSql(sql);
            return newscount;

        }
        //添加资讯
        public void addNews(News ns)
        {
            string sql = "select top 1 * from news order by NewId";
            DataSet ds = db.GetDataSet(sql);
            //创建新的一行
            DataRow dr = ds.Tables[0].NewRow();
            //NewId, Title, Date, Content, Picture
            dr["Title"] = ns.Title;
            dr["Date"] = DateTime.Now;
            dr["Content"] = ns.Content;
            dr["Picture"] = ns.Picture;
            ds.Tables[0].Rows.Add(dr);
            db.UpdateDataset(sql, ds);


        }
        //更新资讯
        //public int updateNews(News ns)
        //{
        //    //string sql = "update news"+ "set title='"+ns.Title+"',"+"body='"+ns.Body+"'"+"where newsId="+ns.Newsid;
        //    string sqll = "update news set title='" + ns.Title + "',Content='" + ns.Content + "',picture='" + ns.Picture + "' where newId='" + ns.NewId + "'";
        //    int i = db.ExecSql(sqll);
        //    return i;
        //}

        #endregion


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuiYangPageModel
{
    #region Model
    public class CpyAdmin
    {
        private int _adminId;

        public int AdminId
        {
            get { return _adminId; }
            set { _adminId = value; }
        }
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        private string _telePhone;

        public string TelePhone
        {
            get { return _telePhone; }
            set { _telePhone = value; }
        }
        private string _mobilePhone;

        public string MobilePhone
        {
            get { return _mobilePhone; }
            set { _mobilePhone = value; }
        }
    }
    #endregion
}

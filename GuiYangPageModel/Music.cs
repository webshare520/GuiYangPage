﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuiYangPageModel
{
  public  class Music
    {
        public int Id { get; set; }
        public string  Name { get; set; }
        public string  Src { get; set; }
        public DateTime Date { get; set; }
        public string Img { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GuiYangPageDal;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using GuiYangPageModel;
namespace GuiYangPageBll
{
    public class NewsBll
    {
        NewsDal newsDal = new NewsDal();

        /// <summary>
        /// 增加一条新闻
        /// </summary>
        /// <param name="news"></param>
        //public void AddNewsinfo(News news)
        //{
        //    newsDal.AddNewsinfo(news);
        //}             
        #region 同步刷新 分页方法
        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return newsDal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return newsDal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<News> DataTableToList(DataTable dt)
        {
            List<News> modelList = new List<News>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                News model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = newsDal.DataRowToModel(dt.Rows[n]);
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }
        #endregion

        ////分页得到新闻
        //public DataSet getNews()
        //{
        //    return newsDal.getNews();
        //}
        public DataSet getNewsByPage(int pageNo, int pageSize, string title)
        {
            return newsDal.getNewsByPage(pageNo, pageSize, title);
        }
        //得到新闻的数量
        public int getNewsCount(string title)
        {
            return newsDal.getNewsCount(title);
        }
        //根据资讯ID删除资讯内容
        public int deleteNews(string[] ids)
        {
            return newsDal.deleteNews(ids);
        }
        //添加资讯
        public void addNews(News ns)
        {
            newsDal.addNews(ns);
        }

    }
}

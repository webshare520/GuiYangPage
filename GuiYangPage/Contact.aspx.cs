﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using GuiYangPageBll;
using GuiYangPageDal;
namespace GuiYangPage
{
    public partial class Contact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {             
                //读取公司联系人信息
                CpyAdminBll bll = new CpyAdminBll();
                SqlDataReader reader = bll.GetProAdmin();
                string name = "", tel = "", mbPhone = "";
                while (reader.Read())
                {
                    name = name + reader["Name"].ToString().Trim();
                    tel = tel + reader["Telephone"].ToString().Trim();
                    mbPhone = mbPhone + reader["MobilePhone"].ToString().Trim();
                }
                lanAdmin.Text = name;
                LabTel.Text = tel;
                LabPhone.Text = mbPhone;

            }
        }
    }
}
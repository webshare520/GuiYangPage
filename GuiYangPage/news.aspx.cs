﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GuiYangPageBll;
using GuiYangPageDal;
using GuiYangPageModel;
using Common;
namespace GuiYangPage
{
    public partial class news : System.Web.UI.Page
    {
        public string NavString { get; set; }
        public int Count { get; set; }
        public int PageCount { get; set; }
        public List<News> newsList { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
             //拿到你传来的页码的参数===当前页
            int pageIndex= int.Parse(Request["pageIndex"] ?? "1");
            int pageSize = 1;//只需要读取一条
           // int total = 0;
            NewsBll bll = new NewsBll();
            //取当前页的数据
            //(pageIndex - 1) * pageSize + 1  ==>当前页
            //pageSize * pageIndex ==>当前要显示的条数
            var ds = bll.GetListByPage(string.Empty, "NewId", (pageIndex - 1) * pageSize + 1, pageSize * pageIndex);
   
           newsList=bll.DataTableToList(ds.Tables[0]);
            //设置一共多少页
            var allCount= bll.GetRecordCount(string.Empty);
            Count = allCount;
            PageCount = Math.Max((allCount + pageSize - 1) / pageSize, 1);

            //生成 分页的标签
          NavString= Common.LaomaPager.ShowPageNavigate(pageSize, pageIndex, allCount);
        }
       
    }
}
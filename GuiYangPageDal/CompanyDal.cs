﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using GuiYangPageModel;
namespace GuiYangPageDal
{
  public  class CompanyDal
    {
       DatabaseSql db;
       public CompanyDal()
       {
        db = new DatabaseSql();
       }

       public List<Company> GetCompanyInfo()
       {
           string sql = "select * from company";
           DataTable dt=  db.ExecuteDataTable(sql, CommandType.Text);
           List<Company> list = new List<Company>();
             Company commpany=null;
           for (int i =  0; i < dt.Rows.Count; i++)
           {
              commpany = new Company();
               commpany.Description = dt.Rows[i]["Description"] != null ? dt.Rows[i]["Description"] as string : string.Empty;               
           }
           list.Add(commpany);
           return list;
       }
     
    }
}

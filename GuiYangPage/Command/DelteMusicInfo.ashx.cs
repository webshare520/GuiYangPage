﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GuiYangPageDal;
namespace GuiYangPage.Command
{
    /// <summary>
    /// DelteMusicInfo 的摘要说明
    /// </summary>
    public class DelteMusicInfo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            int id = int.Parse(context.Request["id"].ToString());
            MusicDal dal = new MusicDal();
            int i = dal.DeleteMusicInfo(id);
            if (i > 0)
            {
               context. Response.Write("ok");
            }
            else
            {
              context. Response.Write("no");
            }
          //  context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
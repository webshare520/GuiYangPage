﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace Common
{
  public static  class SqlHelper
    {
         //定义一个连接字符串
        //readonly修饰的变量只能在初始化的时候赋值，或构造函数中赋值，其他地方只能读取，不能赋值。
      private static readonly string constr=ConfigurationManager.ConnectionStrings["mssqlserver"].ConnectionString;       
     /// <summary>
     /// 执行增删改
     /// </summary>
     /// <param name="sql">定义的sql语句</param>
     /// <param name="cmdType">确定执行的是存储过程还是sql文本</param>
     /// <param name="pms">参数数组</param>
     /// <returns></returns>
      public static int ExecuteNonQuery(string sql,CommandType cmdType,params SqlParameter[] pms)
      {
          using (SqlConnection con=new SqlConnection(constr))
          {
                    using (SqlCommand cmd=new SqlCommand(sql,con))  
                    {
                        cmd.CommandType=cmdType;
                        if(pms!=null)
                        {
                            cmd.Parameters.AddRange(pms);
                        }
                        con.Open();
                        return cmd.ExecuteNonQuery();
                    }
          }
 
      }
           //封装返回单个值的
      public static object ExecuteScalar(string  sql,CommandType cmdType,params SqlParameter[] pms)
      {
        using(SqlConnection con=new SqlConnection (constr))
	    {
            using(SqlCommand cmd=new SqlCommand(sql,con))
            {
                cmd.CommandType=cmdType;
                if(pms!=null)
                {
                    cmd.Parameters.AddRange(pms);
                }
                con.Open();
                return cmd.ExecuteScalar();
            }
		}

     }
        //执行返回DataReader方法
      public static SqlDataReader ExecuteReader(string sql,CommandType cmdType,params SqlParameter[] pms)
      {
         using(SqlConnection con=new SqlConnection (constr))
	    {
             using(SqlCommand cmd=new SqlCommand (sql,con))
	        {
                 cmd.CommandType=cmdType;
                 if(pms!=null)
                 {
                     cmd.Parameters.AddRange(pms);
                 }
                 try 
	            {	        
		             con.Open();
                     return cmd.ExecuteReader(CommandBehavior.CloseConnection);
	            }
	         catch (Exception)
	            {
		        con.Close();
                 con.Dispose();//释放资源
		         throw;
	             }
		 
	        }		 
	    }
      }
    //封装返回DataTable的方法
      public static DataTable ExcuteDataTable(string sql,CommandType cmdType,params SqlParameter[]pms)
      {
          DataTable dt=new DataTable();
            using(SqlDataAdapter adapter=new SqlDataAdapter(sql,constr))
            {
                adapter.SelectCommand.CommandType=cmdType;
                if(pms!=null)
                {
                    adapter.SelectCommand.Parameters.AddRange(pms);
                }
            adapter.Fill(dt);
            }
         
          return dt;

     }
      /// <summary>
      /// 用数据集的方式获得数据列表
      /// </summary>
      /// <param name="SQLString"></param>
      /// <param name="cmdParms"></param>
      /// <returns></returns>
      public static DataSet GetQuery(string sql, params SqlParameter[] pms)
      {
         
          using (SqlDataAdapter adapter=new SqlDataAdapter(sql,constr))
          {
             // adapter.SelectCommand.CommandType = cmdType;
              DataSet ds = new DataSet();
              if (pms != null)
              {
                  adapter.SelectCommand.Parameters.AddRange(pms);
              }
              adapter.Fill(ds);
              return ds;
          }
      }
      public static DataSet Query(string sql)
      {
          using (SqlConnection con = new SqlConnection(constr))
          {
              DataSet ds = new DataSet();
              try
              {
                  con.Open();
                  SqlDataAdapter command = new SqlDataAdapter(sql, con);
                  command.Fill(ds, "ds");
              }
              catch (System.Data.SqlClient.SqlException ex)
              {
                  throw new Exception(ex.Message);
              }
              return ds;
          }
      }
      /// <summary>
      /// 执行一条计算查询结果语句，返回查询结果（object）。
      /// </summary>
      /// <param name="SQLString">计算查询结果语句</param>
      /// <returns>查询结果（object）</returns>
      public static object GetSingle(string sql)
      {
          using (SqlConnection con = new SqlConnection(constr))
          {
              using (SqlCommand cmd = new SqlCommand(sql, con))
              {
                  try
                  {
                      con.Open();
                      object obj = cmd.ExecuteScalar();
                      if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                      {
                          return null;
                      }
                      else
                      {
                          return obj;
                      }
                  }
                  catch (System.Data.SqlClient.SqlException e)
                  {
                      con.Close();
                      throw e;
                  }
              }
          }
      }
          
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadMp3.aspx.cs" Inherits="GuiYangPage.UploadMp3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>贵阳科创科技发展有限公司-后台管理</title>

    <link href="Script/css/tableStyle.css" rel="stylesheet" />

    <link href="/easyUI/themes/default/easyui.css" rel="stylesheet" />
    <link href="easyUI/themes/color.css" rel="stylesheet" />
    <link href="/easyUI/themes/icon.css" rel="stylesheet" />
    <script src="/easyUI/jquery.min.js"></script>
    <script src="/easyUI/jquery.easyui.min.js"></script>
    <script src="/Script/js/jquery-form.js"></script>
    <style>
        a:link {
            color: #000;
            text-decoration: none;
        }

        a:visited {
            color: #000;
        }

        a:hover {
            color: #0E2D5F;
        }
    </style>
    <%-- 使用easyui时 进入一个新页面 前要经过一个页面混乱的时..利用这个onComplete 事件在结合一个载入遮罩层就解决问题了--%>
    <script type="text/javascript">
        function closes() {
            $("#Loading").fadeOut("normal", function () {
                $(this).remove();
            });
        }
        var pc;
        $.parser.onComplete = function () {
            if (pc) clearTimeout(pc);
            pc = setTimeout(closes, 1000);
        }
    </script>
</head>
<body class="easyui-layout">
    <%--  加上遮罩层开始--%>
    <div id='Loading' style="position: absolute; z-index: 1000; top: 0px; left: 0px; width: 100%; height: 100%; background: white; text-align: center; padding-top: 20%;">
        <h1>
            <image src='img/loading.gif' style="height: 120px;" />
        </h1>
    </div>
    <%--  加上遮罩层结束--%>

    <div data-options="region:'north',border:false" style="height: 90px; padding: 5px; overflow: hidden; background-image: url(img/tl.jpg); background-size: 100% 100%;">


        <div style="height: 100px; float: right; text-align: right; padding-right: 50px; overflow: hidden">
            <br />
            <span><a href="index.aspx" style="margin-right: 20px">回到首页</a><a href="us.aspx" style="margin-right: 20px">联系我们</a><a href="javascript:void(0)">客户投诉</a></span>
            <br />
            <br />
            <span style="float: right; font-size: 15px;"><%= DateTime.Now.ToString("D")%> </span>
        </div>

    </div>
    <div data-options="region:'center',title:'贵阳科创科技后台数据管理主窗口'">
        <div id="tb" style="background: #829697">
            <div>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconcls="icon-add" plain="true" onclick="obj.add()">添加</a>


            </div>
        </div>
        <table id="MusicInfo" style="text-align: center; width: 100%">

            <tr style="height: 30px; width: 300px;">
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <td>编号</td>
                <td>歌名</td>
                <td>路径</td>
                <td>时间</td>
                <td>操作</td>
            </tr>
            <%foreach (var music in listMusic)
              {%>
            <tr>
                <td><%=music.Id %></td>
                <td><%=music.Name %></td>
                <td><%=music.Src %></td>
                <td><%=music.Date.ToString("d")%></td>
                <td><a href="javascript:void(0)" class="deletes" ids="<%=music.Id %>"><span style="color: #fa0a0a; font-weight: 400; font-size: medium">删&nbsp;除</span></a></td>
            </tr>
            <%  } %>
        </table>
        <div id="dlg" class="easyui-dialog" title="音乐添加" style="width: 500px; height: 300px; padding: 10px 20px" data-options="closed:true,iconCls:'icon-add'">
            <div class="ftitle">
                <b>音乐产品</b>
                <hr />
            </div>
            <form id="frm" method="post" action="Command/AddPricture.ashx/ProcessRequest">
                <table>

                    <tr style="height: 10px;"></tr>
                    <tr class="fitem">
                        <td style="width: 100px">歌名：</td>
                        <td style="width: 500px">
                            <input type="text" class="easyui-validatebox textbox" name="musicName" id="musicName" style="width: 280px; height: 25px;" data-options="prompt:'请输入音乐名字...',required:true,missingMessage:'歌名必须输入！'" /></td>
                    </tr>
                    <tr style="height: 10px;"></tr>
                    <tr>
                        <td>文件：</td>
                        <td>
                            <input class="easyui-filebox" name="file1" style="width: 280px; height: 25px;" data-options="prompt:'请选择要上传的音乐文件'" />
                        </td>
                    </tr>
                    <tr style="height: 10px;"></tr>

                </table>
                <div id="dlg-buttons" style="text-align: center; padding: 50px 20px 30px  0">
                    <input style="height: 27px; width: 75px;" type="submit" class="easyui-linkbutton c1" value=" 确 定" />
                    <a href="javascript:void(0)" style="height: 25px; width: 75px;" class="easyui-linkbutton c4" onclick="obj.close()">关闭</a>
                </div>
            </form>
        </div>
    </div>
    <div data-options="region:'west',split:true,title:'功能'" style="width: 175px; padding: 10px; overflow: hidden; background: #92e4ef">

        <div class="easyui-accordion" data-options="multiple:true" style="width: 160px; text-align: center; background: #92e4ef">

            <div title="产品管理" data-options="iconCls:'icon-ok'" style="overflow: hidden; padding: 10px; background: #92e4ef">
                <a href="AddProductUpload.aspx" class="easyui-linkbutton c5" data-options="plain:true" style="width: 140px; text-align: center">产品列表</a><br />
            </div>
            <div title="新闻管理" data-options="iconCls:'icon-ok'" style="overflow: hidden; padding: 10px; background: #92e4ef">
                <a href="backgroundUpload.aspx" class="easyui-linkbutton c3" data-options="plain:true" style="width: 140px; text-align: center">新闻列表</a><br />
            </div>
            <div title="音乐管理" data-options="iconCls:'icon-ok'" style="overflow: hidden; padding: 10px; background: #92e4ef">
                <a href="UploadMp3.aspx" class="easyui-linkbutton c6" data-options="plain:true" style="width: 140px; text-align: center">音乐列表</a><br />
            </div>
        </div>
    </div>
    <div data-options="region:'south',border:false" style="height: 60px; background: #191818; padding: 10px; text-align: center;">
        <font style="color: #ffffff; font-size: large">  Copyright ? 版权所有 2017 - 2018 贵阳科创科技有限公司      © 2015 LightMagic,Inc 黔ICP备16011091号（2016）  </font>
    </div>



    <script type="text/javascript">
        $(function () {
            $('.deletes').click(function () {
                deleteMusicInfo($(this).attr("ids"), $(this));
            });
        })
        //删除新闻信息
        function deleteMusicInfo(id, control) {
            $.messager.confirm("温馨提示", "您确定要删除这条记录吗？", function (r) {
                if (r) {
                    $.post("Command/DelteMusicInfo.ashx", { "id": id }, function (data) {
                        if (data == "ok") {
                            $.messager.alert("提示", "删除成功!", "info");
                            $(control).parent().parent().remove();
                        } else {
                            $.messager.alert("提示", "删除失败!", "info");
                        }
                    });
                }
            });
        }
        //弹出div
        var obj = {
            add: function () {
                $('#dlg').dialog('open').dialog('setTitle', '添加');
                $('#frm').form('clear');
            },
            close: function () {
                $('#dlg').dialog('close');
            }
        }
        //=========================表单上传文件=====================
        $('#frm').ajaxForm({
            url: 'Command/AddMusicInfo.ashx',
            beforeSerialize: function () {
            },
            error: function (result) {
                alert(result);
                // console.log("Error")
            },
            success: function (result) {
                alert(result)
                // console.log(result);
                //console.log("success");
                window.location.href = "UploadMp3.aspx";
            }
        });
    </script>
</body>
</html>

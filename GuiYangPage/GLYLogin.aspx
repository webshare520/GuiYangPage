﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GLYLogin.aspx.cs" Inherits="GuiYangPage.GLYLogin" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="Script/js/jquery-1.7.1.js"></script>
    <style>
        * {
            margin: 0;
            padding: 0;
            font-family: 华文新魏;
        }

        body {
            background-image: url("img/background.jpg");
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
        }

        form {
            position: absolute;
            top: 34%;
            right: 35%;
            width: 300px;
            height: 210px;
            background: rgba(255,255,255,0.28);
            z-index: 10;
            text-align: left;
            padding: 40px 90px;
            line-height: 40px;
        }
        form p{
            text-align:center;
        }
        form div{
            width:244px;
            margin:0 auto;
        }
            form input[type="text"] {
                background-color: transparent;
                border: 1px solid #5a5555;
                width: 212px;
                height: 25px;
                font-size: 15px;
            }

            form input[type="password"] {
                background-color: transparent;
                border: 1px solid #5a5555;
                width: 212px;
                height: 25px;
                font-size: 15px;
            }

            form input[type="button"] {
                width: 100px;
                height: 30px;
                margin: 20px 10px 0;
                font-size: 16px;
                background-color: transparent;
                border: 1px solid #5a5555;
            }

            form p {
                font-weight: bolder;
                font-size: 20px;
            }
    </style>
    <script type="text/javascript">
        ////定义一个函数 用户名文本框得到焦点时执行
        //function gotFocus() {
        //    //得到用户名文本框
        //    if (document.getElementById("txtName").value == "请输入用户名") {
        //        //清空文本框的内容
        //        document.getElementById("txtName").value = "";
        //    }
        //}
        //function lostFocus() {
        //    if (document.getElementById("txtName").value == "") {
        //        document.getElementById("txtName").value = "请输入用户名";
        //    }
        //}
        //登陆
        $(function () {
            $('#btnOk').click(function () {
                $.ajax({
                    url: "../Command/GetLogin.ashx",
                    type: 'post',
                    data: {
                        username: $('#txtName').val(),
                        password: $('#txtPwd').val(),
                        code: $('#txtCode').val()
                    },
                    datatype: "text",
                    success: function (data) {
                        if (data == "errorCode") {
                            alert("验证码错误！");
                            var input_a = document.getElementById("txtName").value = "";
                            var input_b = document.getElementById("txtPwd").value = "";
                            var input_c = document.getElementById("txtCode").value = ""
                        }
                        else if (data == "1") {
                            alert("登录成功！");
                            window.location.href = "backgroundUpload.aspx";
                        }
                        else if (data == "0") {
                            alert("登录失败！");
                            // window.location.href = "GLYLogin.aspx";
                            var input_a = document.getElementById("txtName").value = "";
                            var input_b = document.getElementById("txtPwd").value = "";
                            var input_c = document.getElementById("txtCode").value = ""
                            // $("#form1").form('clear');
                        }
                        else {
                            alert("用户名或密码不能为空！");
                            $("#form1").form('clear');
                        }
                    }
                });
            });
        });
        $(function () {
            $('#btnCance').click(function () {
                confirm("您确定取消登录吗？");
                window.location.href = "GLYLogin.aspx";

                // $('#form1').html('');清空表单
            });
        });
        //验证码的点击变换事件
        $(function () {
            $("#imgCode").click(function () {
                var oldSrc = $(this).attr("src");
                oldSrc += new Date().toString();
                $(this).attr("src", oldSrc);
            });
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">

        <p>后台数据管理系统登录</p>
        <lable>用户名:</lable>
        <input type="text" id="txtName" placeholder="请输入管理员名" />
        <br>
        <lable>密&nbsp;&nbsp;码:</lable>
        <input type="password" id="txtPwd" placeholder="请输入管理员密码" />
        <br>
        <lable>验证码:</lable>
        <input type="text" id="txtCode" name="txtCode" style="width: 57px;" />
        <img id="imgCode" src="Command/ValidateCodeImageHandler.ashx?id=3" style="border-width: 0px; margin: 0 0 -6px 20px" /><br />
        <div>
            <input type="button" id="btnOk" value="登录" />
            <input type="button" id="btnCance" value="取消" />
        </div>
       

    </form>
</body>
</html>

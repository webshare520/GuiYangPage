﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="news.aspx.cs" Inherits="GuiYangPage.news" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>贵阳科创科技发展有限公司-新闻中心</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="description" content="贵阳科创科技发展有限公司于2007年2月正式注册成立，是专业为科技馆、博物馆、纪念馆、规划馆、游乐园，展示厅等提供创意策划、设计及项目建设的民营高新技术企业 、软件企业。" />
    <meta name="keywords" content="球幕影院,360度环幕影院,弧幕影院,互动多媒体,镜子迷宫,时空隧道,动感球幕影院,模拟飞行器,时光隧道,八卦迷宫,豪华镜子迷宫
" />
    <script src="Script/js/jquery.js"></script>
    <script src="Script/js/main.js"></script>
    <link href="Script/css/main.css" rel="stylesheet" />
    <link href="Script/css/news.css" rel="stylesheet" />
    <link href="Script/css/NavPager.css" rel="stylesheet" />
    <link href="Script/css/style_m.css" rel="stylesheet" />
    <link href="Script/css/media.css" rel="stylesheet" />
    <script type="text/javascript">
        $(function () {
            $('#more').click(function () {
                return confirm("需要上传新闻？请先去登陆！");
            });
        });
    </script>
</head>
<body>

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>    
       
        <div class="nav ">
            <div class="content">
                <div class="logo">
                    <img src="./img/logo.png" alt="" />
                </div>
                <div class="nav-name">
                    <ul>
                        <li><a href="index.aspx">主页</a></li>
                        <li><a href="product.aspx">产品</a></li>
                        <li><a href="case.aspx">案例</a></li>
                        <li><a href="news.aspx" class="active">新闻</a></li>
                        <li><a href="us.aspx">关于我们</a></li>
                    </ul>
                   
                </div>
                <div class="both"></div>
            </div>
        </div>
        <div class="mad">
        </div>
        <div class="main ">
            <div class="content">
                <div class="addr">
                    当前位置：<a href="news.aspx">新闻</a>
                </div>
                <div class="main-con">
                    <div class="left">
                        <ul>
                            <li class="first"><a href="news.aspx">新闻中心</a></li>
                            <li class="active"><a href="news.aspx">热点新闻</a></li>
                        </ul>
                    </div>
                    <div class="right" >
                        <ul class="product" style="display:block;">
                            <li>                            
                                    <div class="pic" id="picture">
                                        <%   foreach (var news in newsList)
                                             {%>
                                     <a href="ndetail.aspx?id=<%=news.NewId %>"> <img src="<%=news.Picture %>" alt="#" /></a>
                                    </div>
                                    <div class="text">
                                        <!------------------分页 遍历输出------------------------->

                                        <div class="title" id="DTitle"><%=news.Title %></div>
                                        <div class="con" id="newsInfo"><%=news.Content%></div>
                                        <%} %> 
                                    </div>
                               
                            </li>
                        </ul>
                        <div class="Paging">

                            <!------------------分页 超级链接------------------------->
                            <ul>

                                <%--    <li class="PagActive">
                        <%   for (int i = 1; i <= PageCount; i++)
                       {%>
                        <a href="news.aspx?pageIndex=<%=i %>">第<span><%= i %></span>页</a>                     
                      <%} %> 
                             </li>
                               <li><span class="PageInfo"><span>共</span><%=Count %><span>条</span></li>   --%>

                                <!------------------分页 静态方法生成的分页的标签------------------------->
                            </ul>
                        </div>
                        <br />
                        <div class="paginator">
                            <%= NavString %>    
        `            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="right-qq">
        <a class="pst-rlt" href="tencent://message/?uin=1035578968&Site=&menu=yes">
            <img src="./img/qq-icon-bg.gif" alt="" />
            <div class="text">
                24小时在线解答
            </div>
        </a>
        <a class="pst-rlt online" href="##">
            <img src="./img/right_online.png" alt="" />
            <div class="text">
                热线电话：<br />0851-8511905
            </div>
        </a>
        <a class="pst-rlt online2" href="tencent://message/?uin=1035578968&Site=&menu=yes">
            <img src="./img/right_online.png" alt="" />
            <div class="text">
                24小时在线咨询
            </div>
            </a>
            <div class="weixin pst-rlt">
                微信扫一扫了解更多
            <div class="pic pos-abs">
                <img src="./img/index_bottom_erweima.png" alt="" />
            </div>
            </div>
        </div>
        <div class="bottom-nav">
            <div class="content">
                <div class="b-nav">
                    <div class="name">导航</div>
                    <ul>
                        <li><a href="index.aspx">主页</a><a href="product.aspx">产品</a></li>
                        <li><a href="news.aspx">新闻</a><a href="case.aspx">案例</a></li>
                        <li><a href="us.aspx">关于我们</a></li>

                    </ul>
                </div>
                <div class="info">
                    <ul>
                        <li class="logo">
                            <img src="./img/index_blogo.jpg" alt="" /></li>
                        <li>
                            <img src="./img/companyname.png"  class="info_img" alt="" /></li>
                        <li>
                            <img src="./img/index_bm_phone.jpg" alt="" /><span>+86 0851-8511905</span></li>
                        <li>
                            <img src="./img/index_bm_mail.jpg" alt="" /><span>1035578968@qq.com</span></li>
                        <li class="adress">
                            <img src="./img/index_bm_adress.jpg" alt="" /><span>中国贵州贵阳国家高新技术产业开发区</span></li>
                    </ul>
                </div>
                <div class="weixin">
                    <img src="./img/index_bottom_erweima.png" alt="">
                    <div class="text">官方微信号</div>
                </div>
            </div>
        </div>
              

        <div id="player">
	<div class="cover"></div>
	<div class="ctrl">
		<div class="tag">
			<strong>Title</strong>
			<span class="artist">Artist</span>
			<span class="album">Album</span>
		</div>
		<div class="control">
			<div class="left">
				<div class="rewind icon"></div>
				<div class="playback icon"></div>
				<div class="fastforward icon"></div>
			</div>
			<div class="volume right">
				<div class="mute icon left"></div>
				<div class="slider left">
					<div class="pace"></div>
				</div>
			</div>
		</div>
		<div class="progress">
			<div class="slider">
				<div class="loaded"></div>
				<div class="pace"></div>
			</div>
			<div class="timer left">0:00</div>
			<div class="right">
				<div class="repeat icon"></div>
				<div class="shuffle icon"></div>
			</div>
		</div>
	</div>
</div>



    <div class="footer">
        <div class="content"> © 2015 LightMagic,Inc 黔ICP备16011091号</div>
    </div>
    <script src="Script/js/jquery-ui-1.8.17.custom.min.js"></script>
    <script src="Script/js/script.js"></script>
     
    </form>
</body>

</html>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GuiYangPageBll;
using System.Data;
using GuiYangPageModel;
using System.IO;
namespace GuiYangPage.Command
{
    /// <summary>
    /// GetProductInfo 的摘要说明
    /// </summary>
    public class GetProductInfo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            NewsBll bll = new NewsBll();
            string type = context.Request["type"].ToString();
            #region  //资讯内容
            if (type == "show")
            {
                int pageNo = int.Parse(context.Request["page"].ToString());
                int pageSize = int.Parse(context.Request["rows"].ToString());
                string title = "";
                if (context.Request["title"] != null)
                    title = context.Request["title"].ToString().Trim();
                int count = bll.getNewsCount(title);
                DataSet ds = bll.getNewsByPage(pageNo, pageSize, title);
                string json;
                json = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0], new Newtonsoft.Json.Converters.DataTableConverter());
                json = "{\"total\":" + count.ToString() + ",\"rows\":" + json + "}";
                context.Response.Write(json);
            }
            #endregion
            #region //删除资讯
            else if (type == "del")
            {
                string ids = context.Request["id"].ToString();
                string[] id = ids.Split(',');
                int count = bll.deleteNews(id);
                context.Response.Write(count);
            }
            #endregion
          
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
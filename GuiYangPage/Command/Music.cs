﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuiYangPage.Command
{
    public class MusicTow 
    {
        public string title { get; set; }
        public string  artist { get; set; }

        public string  album { get; set; }
        public string  cover { get; set; }

        public string  mp3 { get; set; }

        public string  ogg { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GuiYangPageDal;
using System.Data.SqlClient;
using GuiYangPageBll;
using GuiYangPageModel;
using System.Data;
namespace GuiYangPage
{
    public partial class index : System.Web.UI.Page
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
         
            if (!IsPostBack)
            {
                  #region  读取数据的low代码
                string sql = "select Introduction from Product ";
                DatabaseSql db = new DatabaseSql();
                SqlDataReader dr = db.GetDataReader(sql);

                string proDesc = "";  
                while (dr.Read())
                {
                   proDesc = proDesc + dr["Introduction"].ToString();                
                }
                labInduct.Text = proDesc;   
            

                string f1 = "", f2 = "", f3 = "", f4 = "";
                
             
                ProductBll bll = new ProductBll();
                    dr=  bll.GetProInfo1();
                 while (dr.Read())
                 {
                     f1 = f1  + dr["FeaText"].ToString().Trim();     
                 }
                 labFatText1.Text = f1;
                 dr = bll.GetProInfo2();
                 while (dr.Read())
                 {
                      f2 = f2 + dr["FeaText"].ToString().Trim();
                 }
                labFatText2.Text = f2;

              dr=  bll.GetProInfo3();
              while (dr.Read())
              {
                   f3 = f3 + dr["FeaText"].ToString().Trim();
              }
              labFatText3.Text = f3;
              dr = bll.GetProInfo4();
              while (dr.Read())
              {
                  f4 = f4 + dr["FeaText"].ToString().Trim();
              }            
             labFatText4.Text = f4;

                //========================读取产品应用信息==========
             string p1 = "", p2 = "", p3 = "";
             dr = bll.GetProYg1();
             while (dr.Read())
             {
                 p1 = p1  + dr["ProContent"].ToString().Trim();
             }
             labeMovie.Text = p1;           
             dr = bll.GetProYg2();
             while (dr.Read())
             {
                 p2 = p2 + dr["ProContent"].ToString().Trim();
             }
           labChe.Text = p2;
             dr = bll.GetProYg3();
             while (dr.Read())
             {
                 p3 = p3 + dr["ProContent"].ToString().Trim();
             }
           labBook.Text = p3;
                  #endregion
            }
        }

      
    }
}
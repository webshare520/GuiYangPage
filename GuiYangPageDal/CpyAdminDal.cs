﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using GuiYangPageModel;
namespace GuiYangPageDal
{
 public   class CpyAdminDal
    {
     DatabaseSql db;
     public CpyAdminDal()
     {
         db = new DatabaseSql();
      }
     /// <summary>
     /// 获取公司联系人信息
     /// </summary>
     /// <returns></returns>
     public SqlDataReader GetProAdmin()
     {
         //where AdminId=5
         string sql = "select top 1 Name,Telephone,MobilePhone from CpyAdmin   ";
         SqlDataReader dr = db.GetDataReader(sql);
         return dr;
     }
     /// <summary>
     /// 用户登录
     /// </summary>
     /// <param name="name"></param>
     /// <param name="pwd"></param>
     /// <returns></returns>
     public int GetLogin(string name, string pwd)
     {
         string sql = "select count(*)from CpyAdmin where Name=@name and Password=@pwd";
         SqlParameter[] pms ={
                            new SqlParameter("@name",SqlDbType.NChar, 20),
                              new SqlParameter("@pwd",SqlDbType.NVarChar, 20),                             
                             };
         pms[0].Value = name;
         pms[1].Value = pwd;
         //return (int)db.ExecuteScalar(sql, CommandType.Text, pms);
         object obj = db.ExecuteScalar(sql, CommandType.Text, pms);
         return Convert.ToInt32(obj);
     }
       
    }
}

﻿using GuiYangPageModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace GuiYangPageDal
{
 public   class MusicDal
    {
     DatabaseSql db;
     public MusicDal()
     {
         db = new DatabaseSql();
     }
     //上传歌曲
     public int AddMusicInfo(Music music)
     {
         string sql = "insert into Music(Name,Src,Date)values(@Name,@Src,@Date)";
         SqlParameter []pms ={                            
                                 new SqlParameter("@Name",SqlDbType.NVarChar,50){Value=music.Name},
                                 new SqlParameter("@Src",SqlDbType.NVarChar,100){Value=music.Src},
                                 new SqlParameter("@Date",SqlDbType.DateTime){Value=music.Date}
                             };
         return   db.ExecuteNonQuery(sql,System.Data.CommandType.Text,pms); 
     }
     //读取歌曲详细信息
     public List<Music> GetMusicInfo()
     {
         string sql = "select  top 12 * from music order by date desc";
          DataTable dt= db.ExecuteDataTable(sql,CommandType.Text);
          List<Music> list = new List<Music>();
         Music music =null;
          for (int i = 0; i < dt.Rows.Count; i++)
          {

               music = new Music();
              music.Id = int.Parse(dt.Rows[i]["Id"].ToString());
              music.Name=dt.Rows[i]["Name"]!=null?dt.Rows[i]["Name"] as string :string.Empty;
              music.Src = dt.Rows[i]["Src"] != null?dt.Rows[i]["Src"] as string :string.Empty;
              music.Date = dt.Rows[i]["Date"] != DBNull.Value? DateTime.Parse(dt.Rows[i]["Date"].ToString()) : DateTime.Now;
              music.Img = dt.Rows[i]["Img"] != DBNull.Value ? dt.Rows[i]["Img"].ToString() : "img/musicImg2.png";
              list.Add(music);
          }
    
          return list;
     }
     public int DeleteMusicInfo(int id)
     {
         string sql = "delete from music where id=@id";
         SqlParameter[] pms = { 
                              new SqlParameter("@id",SqlDbType.Int){Value=id}
                              };
        return  db.ExecuteNonQuery(sql, CommandType.Text,pms);
 
     }
    }
}

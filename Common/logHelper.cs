﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
namespace Common
{
 public   class logHelper
    {
        public static Queue<string> exceptionInfoQueue = new Queue<string>();
        public static string logBasePath;
        //定义一个静态的构造函数
        static logHelper()
        {
            //提供一个线程池、将提供方法排入队列方便执行
            ThreadPool.QueueUserWorkItem(o =>
            {
                while (true)
                {
                    if (exceptionInfoQueue.Count > 0)
                    {
                        //取出对象集合中的错误赋值于str
                        string str = exceptionInfoQueue.Dequeue();

                        //写入错误消息。
                        string strFileName = DateTime.Now.ToString("yyyy-mm-dd") + ".txt";
                        //江文件路径转换为绝对路径
                        string absoluteFileName = Path.Combine(logBasePath, strFileName);


                        lock (exceptionInfoQueue)
                        {
                            using (FileStream fs = new FileStream(absoluteFileName, FileMode.Append, FileAccess.Write))
                            {
                                byte[] data = Encoding.Default.GetBytes(str);
                                fs.Write(data, 0, data.Length);
                            }
                        }
                    }

                }

            });
        }
    }
}

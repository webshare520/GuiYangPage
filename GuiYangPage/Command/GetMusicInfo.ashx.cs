﻿using GuiYangPageDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GuiYangPage.Command
{
    /// <summary>
    /// GetMusicInfo 的摘要说明
    /// </summary>
    public class GetMusicInfo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            MusicDal dal = new MusicDal();
             var list=  dal.GetMusicInfo();
             List<MusicTow> musiclist = new List<MusicTow>();

             foreach (var item in list)
             {
                 MusicTow music = new MusicTow()
                 {
                     title="贵阳科创科技发展有限公司",
                       mp3=item.Src,
                       cover=item.Img,
                      artist="",
                     album = "",
                 };
                 musiclist.Add(music);
             }
            
            JavaScriptSerializer jsJavaScriptSerializer = new JavaScriptSerializer();
            string jsonStr = jsJavaScriptSerializer.Serialize(musiclist);
  
             context.Response.Write(jsonStr);
          //  context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
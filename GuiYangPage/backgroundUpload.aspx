﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="backgroundUpload.aspx.cs" Inherits="GuiYangPage.backgroundUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>贵阳科创科技发展有限公司-后台管理</title>
    <link href="/easyUI/themes/default/easyui.css" rel="stylesheet" />
    <link href="easyUI/themes/color.css" rel="stylesheet" />
    <link href="/easyUI/themes/icon.css" rel="stylesheet" />
    <script src="/easyUI/jquery.min.js"></script>
    <script src="/easyUI/jquery.easyui.min.js"></script>
    <script src="/Script/js/jquery-form.js"></script>
    <style>
        a:link {
            color: #000;
            text-decoration: none;
        }

        a:visited {
            color: #000;
        }

        a:hover {
            color: #0E2D5F;
        }
    </style>
    <%-- 使用easyui时 进入一个新页面 前要经过一个页面混乱的时..利用这个onComplete 事件在结合一个载入遮罩层就解决问题了--%>
    <script type="text/javascript">
        function closes() {
            $("#Loading").fadeOut("normal", function () {
                $(this).remove();
            });
        }
        var pc;
        $.parser.onComplete = function () {
            if (pc) clearTimeout(pc);
            pc = setTimeout(closes, 1000);
        }
    </script>
</head>
<body class="easyui-layout">
    <%--  加上遮罩层开始--%>
    <div id='Loading' style="position: absolute; z-index: 1000; top: 0px; left: 0px; width: 100%; height: 100%; background: white; text-align: center; padding-top: 20%;">
        <h1>
            <image src='img/loading.gif' style="height: 120px; text-align: center"></image>
        </h1>
    </div>
    <%--  加上遮罩层结束--%>

    <div data-options="region:'north',border:false" style="height: 90px; padding: 5px; overflow: hidden; background-image: url(img/tl.jpg); background-size: 100% 100%;">


        <div style="height: 100px; float: right; text-align: right; padding-right: 50px; overflow: hidden">
            <br />
            <span><a href="index.aspx" style="margin-right: 20px">回到首页</a><a href="us.aspx" style="margin-right: 20px">联系我们</a><a href="javascript:void(0)">客户投诉</a></span>
            <br />
            <br />
            <span style="float: right; font-size: 15px;"><%= DateTime.Now.ToString("D")%> </span>
        </div>

    </div>
    <div data-options="region:'center',title:'贵阳科创科技后台数据管理主窗口'">
        <div id="tb" style="background: #829697">
            <div>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconcls="icon-add" plain="true" onclick="obj.add()">添加</a>

                <a href="javascript:void(0)" class="easyui-linkbutton" iconcls="icon-remove" plain="true" onclick="obj.del()">删除</a>
            </div>
            <div style="padding: 0 0 0 10px;">
                请输入新闻标题：<input type="text" class="textbox" id="desn" style="height: 25px; width: 240px;" data-options="required:true,missingMessage:'请输入新闻标题！'" />
                <a href="javascript:void(0)" class="easyui-linkbutton" iconcls="icon-search" plain="true" onclick="obj.search()">查询</a>
            </div>
        </div>
        <table id="newsInfo"></table>
        <div id="dlg" class="easyui-dialog" title="新闻添加" style="width: 750px; height: 500px; overflow: auto; padding: 10px 20px" data-options="closed:true,iconCls:'icon-edit'">
            <div class="ftitle">
                <b>添加新闻</b>
                <hr />
            </div>
            <form id="frm" method="post">
                <table>
                    <tr class="fitem">
                        <td style="width: 100px">新闻标题</td>
                        <td style="width: 500px">
                            <input type="text" class="easyui-validatebox textbox" name="desn" id="txtDesn" size="40" data-options="required:true,missingMessage:'新闻标题必须添加！'" /></td>
                    </tr>
                    <tr>
                        <td>新闻内容</td>
                        <td>
                            <textarea id="txtBody" name="body" cols="70" rows="15"></textarea></td>
                    </tr>
                    <tr>
                        <td>新闻图片</td>
                        <td>
                            <input class="easyui-filebox" name="file1" data-options="prompt:'请选择...'" style="width: 300px" />
                        </td>
                    </tr>
                </table>
                <div id="dlg-buttons" style="text-align: center; padding: 50px 20px 30px  0">
                    <input type="submit" style="height: 21px; width: 61px" class="easyui-linkbutton c2" value=" 确 定" />
                    <a href="javascript:void(0)" style="height: 20px; width: 60px;" class="easyui-linkbutton c3" onclick="obj.close()">关闭</a>
                </div>
            </form>
        </div>
    </div>
    <div data-options="region:'west',split:true,title:'功能'" style="width: 175px; padding: 10px; overflow: hidden; background: #92e4ef">

        <div class="easyui-accordion" data-options="multiple:true" style="width: 160px; text-align: center; background: #92e4ef">

            <div title="产品管理" data-options="iconCls:'icon-ok'" style="overflow: hidden; padding: 10px; background: #92e4ef">
                <a href="AddProductUpload.aspx" class="easyui-linkbutton c6" data-options="plain:true" style="width: 140px; text-align: center">产品列表</a><br />
            </div>
            <div title="新闻管理" data-options="iconCls:'icon-ok'" style="overflow: hidden; padding: 10px; background: #92e4ef">
                <a href="backgroundUpload.aspx" class="easyui-linkbutton c5" data-options="plain:true" style="width: 140px; text-align: center">新闻列表</a><br />
            </div>
            <div title="音乐管理" data-options="iconCls:'icon-ok'" style="overflow: hidden; padding: 10px; background: #92e4ef">
                <a href="UploadMp3.aspx" class="easyui-linkbutton c6" data-options="plain:true" style="width: 140px; text-align: center">音乐列表</a><br />
            </div>
        </div>
    </div>
    <div data-options="region:'south',border:false" style="height: 60px; background: #191818; padding: 10px; text-align: center;">
        <font style="color: #ffffff; font-size: large">  Copyright ? 版权所有 2017 - 2018 贵阳科创科技有限公司      © 2015 LightMagic,Inc 黔ICP备16011091号（2016）</font>
    </div>
</body>
</html>
<script type="text/javascript">
    var op;
    var newId;//更新操作存放资讯的id
    var obj = {
        //查询
        search: function () {
            $('#newsInfo').datagrid('load', {
                Title: $.trim($('#desn').val())
            });
        },
        //删除资讯
        del: function () {
            var rows = $('#newsInfo').datagrid('getSelections');
            //判断是否选择行
            if (!rows || rows.length == 0) {
                $.messager.alert('提示', '请选择要删除的数据!', 'info');
                return;
            }
            $.messager.confirm('提示', '是否删除选中数据?', function (r) {
                if (!r) {
                    return;
                }
                var ids = "";//新闻编号数
                for (var i = 0; i < rows.length; i++) {

                    if (i == 0)
                        ids = rows[i].NewId;

                    else
                        ids = ids + "," + rows[i].NewId;
                }
                $.ajax({
                    //发送请求的地址
                    url: "/Command/GetProductInfo.ashx?type=del",
                    //请求方式
                    type: 'post',
                    //发 送到服务器的数据。将自动转换为请求字符串格式,必须为 Key/Value 格式
                    data: {
                        id: ids
                    },
                    // 在用ajax请求时，没有返回前会出现前出现一个转动的loading小图标或者“内容加载中..”，用来告知用户正在请求数据。
                    beforeSend: function () {
                        $('#newsInfo').datagrid('loading');
                    },
                    //请求成功后回调函数。这个方法有两个参数：服务器返回数据，返回状态
                    success: function (data) {
                        $('#newsInfo').datagrid('loaded');//隐藏加载状态
                        $('#newsInfo').datagrid('load');//加载重新显示第一页的数据
                        $('#newsInfo').datagrid('unselectAll');
                        $.messager.show({
                            title: '提示',
                            msg: data + '个资讯被删除！',
                            //设置请求超时时间（毫秒）。此设置将覆盖全局设置。
                            timeout: 5000,
                            showType: 'slide',
                        });
                    }
                });

            });

        },
        //save: function () {
        //    $.ajax({
        //        url: "/Command/GetProductInfo.ashx?type=" + op,
        //        type: 'post',
        //        data: {
        //            id: newId,
        //            desn: $('#txtDesn').val(),
        //            body: $('#txtBody').val()
        //        },
        //        beforeSend: function () {
        //            $('#newsInfo').datagrid('loading');
        //        },
        //        success: function (data) {
        //            $('#newsInfo').datagrid('loaded');
        //            $('#newsInfo').datagrid('load');
        //            $('#newsInfo').datagrid('unselectAll');
        //            $.messager.show({
        //                title: '提示',
        //                msg: "1个新闻被" + data + "!",
        //                timeout: 3000,
        //                showType: 'slide'
        //            });
        //            $('#dlg').dialog('close');
        //        }
        //    });
        //},
        // 新增资讯
        add: function () {
            $('#dlg').dialog('open').dialog('setTitle', '添加');
            $('#frm').form('clear');

        },
        //=========================新闻编辑 =====================
        //edit: function () {
        //    var row = $('#newsInfo').datagrid('getSelected');
        //    if (row) {
        //        $('#dlg').dialog('open').dialog('setTitle', '编辑');
        //        newId = row.NewId;
        //        $('#txtDesn').val(row.Title);
        //        $('#txtBody').val(row.Content);
        //        op = 'edit';
        //    } else {
        //        $.messager.alert('提示', '请先选择要编辑的记录！');
        //    }
        //},
        close: function () {
            $('#dlg').dialog('close');
        }
    };


    //$(function () { })
    //=========================表单上传文件=====================
    $('#frm').ajaxForm({
        url: 'Command/addNews.ashx',
        beforeSerialize: function () {

        },
        error: function (result) {
            alert(result);
            // console.log("Error")
        },
        success: function (result) {
            alert(result)
            // console.log(result);
            //console.log("success");
            window.location.href = "backgroundUpload.aspx";
        }
    });
    //=========================分页展示=====================
    $(function () {
        $('#newsInfo').datagrid({
            title: '新闻列表',
            idField: 'NewId',
            height: 600,
            loadMsg: '正在加载,请稍候...',
            url: "/Command/GetProductInfo.ashx?type=show",
            striped: false,//是否显示斑马线
            columns: [[
                { field: 'ck', checkbox: true },
           { field: 'NewId', title: '编号', hidden: "false" },
           { field: 'Title', title: '标题', width: 150 },
           { field: 'Content', title: '内容', width: 650 },
           { field: 'Date', title: '时间', width: 170 },
           { field: 'Picture', title: '图片', width: 200 },
            ]],
            nowrap: false,//是否把数据显示在一行上
            toolbar: '#tb',
            //singleSelect: false,               
            rownumbers: true,//行号  
            pageSize: 5,
            pageList: [5, 10, 15, 20, 25],
            pagination: true,
        });
        //设置分页控件       
        var p = $('#newsInfo').datagrid('getPager');
        $(p).pagination({
            beforePageText: '第',//页数文本框前显示的汉字           
            afterPageText: '页    共 {pages} 页',
            displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'
        });
    });
</script>


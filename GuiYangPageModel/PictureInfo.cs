﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuiYangPageModel
{
  public class PictureInfo
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _pricture;

        public string Pricture
        {
            get { return _pricture; }
            set { _pricture = value; }
        }
        private string _price;

        public string Price
        {
            get { return _price; }
            set { _price = value; }
        }
        private string _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        private DateTime _date;

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }
    }
}

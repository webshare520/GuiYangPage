﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GuiYangPageBll;
using  GuiYangPageModel;
using Common;
namespace GuiYangPage
{
    public partial class product : System.Web.UI.Page
    {
        //创建属性字段
        public int Count { get; set; }
        public int PageCount { get; set; }
        public List <PictureInfo> pictureList { get; set; }
        public string NavString { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //拿到你传来的页码的参数
            int pageIndex = int.Parse(Request["pageIndex"] ?? "1");
            int pageSize = 8;//读取8条
           // int total = 0;
           PictureInfoBll bll = new PictureInfoBll();
            ////取当前页的数据
            var ds = bll.GetListByPage(string.Empty, "Id", (pageIndex - 1) * pageSize + 1, pageSize * pageIndex);
             pictureList = bll.DataTableToList(ds.Tables[0]);
             //设置一共多少页
             var allCount = bll.GetRecordCount(string.Empty);
             Count = allCount;
             PageCount = Math.Max((allCount + pageSize - 1) / pageSize, 1);
             //生成 分页的标签
           NavString=  Common.LaomaPager.ShowPageNavigate(pageSize, pageIndex, allCount);
        }
        
    }
}
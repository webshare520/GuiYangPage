﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using GuiYangPageBll;
using GuiYangPageDal;
using GuiYangPageModel;
namespace GuiYangPage
{
    public partial class ndetail : System.Web.UI.Page
    {
        public News NewsInfo { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            int id = int.Parse(Request["id"] ?? "1");
            NewsDal dal = new NewsDal();
               NewsInfo= dal.NewsModel(id);           
        }
    }
}
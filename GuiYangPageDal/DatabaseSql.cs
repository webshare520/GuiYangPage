﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
namespace GuiYangPageDal
{
    public class DatabaseSql
    {

        private string constr = "Data Source=bds256641103.my3w.com;Initial Catalog=bds256641103_db;User Id=bds256641103;Password=bds256641103q;MultipleActiveResultSets=True";
        //private string constr = "Data Source=.;Initial Catalog=GuiYangPage;Integrated Security=True;MultipleActiveResultSets=True";
            private SqlConnection con;
            public DatabaseSql()
            {
                con = new SqlConnection(constr);
                con.Open();
               
                //TODO: 在此处添加构造函数逻辑
               
            }
            public SqlDataReader GetDataReader(string sql)
            {
                using ( SqlCommand com = new SqlCommand(sql,con))
                {
                    try
                    {
                    SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                        return dr;
                    }
                    catch (Exception)
                    {
                        con.Close();
                        con.Dispose();//释放资源
                        throw;
                    }
                  
                }           
               // com.Connection = con;
               // com.CommandText = sql;
               
            }
            //执行无返回结果的Sql命令
            public int ExecSql(string sql)
            {
               
                SqlCommand com = new SqlCommand(sql, con);
                int i = 0;
                i = com.ExecuteNonQuery();//受影响的行数
                return i;
            }
            //执行SQL命令，返回数据集
            public DataSet GetDataSet(string sql)
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, con);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
            public  DataSet Query(string sql)
            {
                using (SqlConnection con = new SqlConnection(constr))
                {
                    DataSet ds = new DataSet();
                    try
                    {
                        con.Open();
                        SqlDataAdapter command = new SqlDataAdapter(sql, con);
                        command.Fill(ds, "ds");
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    return ds;
                }
            }
            /// <summary>
            /// 执行一条计算查询结果语句，返回查询结果（object）。
            /// </summary>
            /// <param name="SQLString">计算查询结果语句</param>
            /// <returns>查询结果（object）</returns>
            public  object GetSingle(string sql)
            {
                using (SqlConnection con = new SqlConnection(constr))
                {
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        try
                        {
                            con.Open();
                            object obj = cmd.ExecuteScalar();
                            if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                            {
                                return null;
                            }
                            else
                            {
                                return obj;
                            }
                        }
                        catch (System.Data.SqlClient.SqlException e)
                        {
                            con.Close();
                            throw e;
                        }
                    }
                }
            }
            public void UpdateDataset(string sql, DataSet ds)
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, con);
                SqlCommandBuilder CmdBuilder = new SqlCommandBuilder(da);//用来自动产生sql语句
                da.UpdateCommand = CmdBuilder.GetUpdateCommand();
                da.InsertCommand = CmdBuilder.GetInsertCommand();
                da.DeleteCommand = CmdBuilder.GetDeleteCommand();
                da.Update(ds);
                da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            }
            public void close()
            {
                con.Close();
            }

            public DataSet GetDataTableBySql(string sql)
            {
                throw new NotImplementedException();
            }
            /// <summary>
            /// 执行增删改
            /// </summary>
            /// <param name="sql">定义的sql语句</param>
            /// <param name="cmdType">确定执行的是存储过程还是sql文本</param>
            /// <param name="pms">参数数组</param>
            /// <returns></returns>
            public  int ExecuteNonQuery(string sql, CommandType cmdType, params SqlParameter[] pms)
            {
                using (SqlConnection con = new SqlConnection(constr))
                {
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        cmd.CommandType = cmdType;
                        if (pms != null)
                        {
                            cmd.Parameters.AddRange(pms);
                        }
                        con.Open();
                        return cmd.ExecuteNonQuery();
                    }
                }

            }

            //封装返回单个值的
            public object ExecuteScalar(string sql, CommandType cmdType, params SqlParameter[] pms)
            {
                using (SqlConnection con = new SqlConnection(constr))
                {
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        cmd.CommandType = cmdType;
                        if (pms != null)
                        {
                            cmd.Parameters.AddRange(pms);
                        }
                        con.Open();
                        return cmd.ExecuteScalar();
                    }
                }

            }
            //执行返回DataReader方法
            public  SqlDataReader ExecuteReader(string sql, CommandType cmdType, params SqlParameter[] pms)
            {
                using (SqlConnection con = new SqlConnection(constr))
                {
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        cmd.CommandType = cmdType;
                        if (pms != null)
                        {
                            cmd.Parameters.AddRange(pms);
                        }
                        try
                        {
                            con.Open();
                            return cmd.ExecuteReader(CommandBehavior.CloseConnection);
                        }
                        catch (Exception)
                        {
                           con.Close();
                            con.Dispose();//释放资源
                            throw;
                        }

                    }
                }
            }
            //封装返回DataTable的方法
            public  DataTable ExecuteDataTable(string sql, CommandType cmdType, params SqlParameter[] pms)
            {
                DataTable dt = new DataTable();
                using (SqlDataAdapter adapter = new SqlDataAdapter(sql, constr))
                {
                    adapter.SelectCommand.CommandType = cmdType;
                    if (pms != null)
                    {
                        adapter.SelectCommand.Parameters.AddRange(pms);
                    }
                    adapter.Fill(dt);
                }
                return dt;
            }

    }
}


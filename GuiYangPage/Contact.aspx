﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="GuiYangPage.Contact" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>贵阳科创科技发展有限公司-联系我们</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="description" content="贵阳科创科技发展有限公司于2007年2月正式注册成立，是专业为科技馆、博物馆、纪念馆、规划馆、游乐园，展示厅等提供创意策划、设计及项目建设的民营高新技术企业 、软件企业。" />
    <meta name="keywords" content="球幕影院,360度环幕影院,弧幕影院,互动多媒体,镜子迷宫,时空隧道,动感球幕影院,模拟飞行器,时光隧道,八卦迷宫,豪华镜子迷宫
" />
    <script src="Script/js/jquery.js"></script>
    <script src="Script/js/main.js"></script>
    <link href="Script/css/main.css" rel="stylesheet" />
    <link href="Script/css/us.css" rel="stylesheet" />
    <link href="Script/css/style_m.css" rel="stylesheet" />
    <link href="Script/css/media.css" rel="stylesheet" />
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=Tdr0jmm7LQjhdpufhorDqTW7lrEAtuzG"></script>
</head>
<body>
    
    
<div class="nav ">
    <div class="content">
        <div class="logo">
            <img src="./img/logo.png" alt="" />
        </div>
        <div class="nav-name">
            <ul>
                <li><a href="index.aspx">主页</a></li>
                <li><a href="product.aspx">产品</a></li>
                <li><a href="case.aspx">案例</a></li>
                <li><a href="news.aspx">新闻</a></li>
                <li><a href="us.aspx" class="active">关于我们</a></li>
            </ul>
        </div>
        <div class="both"></div>
    </div>
</div>
<div class="mad">
</div>
<div class="main ">
    <div class="content">
        <div class="addr">
            当前位置：<a href="us.aspx">关于我们</a>
        </div>
        <div class="main-con">
            <div class="left">
                <ul>
                    <li class="first"><a href="us.aspx">关于我们</a></li>
                    <li class="active"><a href="us.aspx">公司概况</a></li>
                    <li class="active"><a href="contact.aspx">联系我们</a></li>
                </ul>
            </div>
            <div class="right">
                <ul class="product">
                    <li>
                        <div class="title">
                            联系我们
                        </div>

                        <div class="text">联 系 人：
                                <asp:Label ID="lanAdmin" runat="server"></asp:Label>
                            </div>
                            <div class="text">电&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 话   ：<asp:Label ID="LabPhone" runat="server"></asp:Label></div>
                            <div class="text">移动电话：<asp:Label ID="LabTel" runat="server"></asp:Label></div>                     
                        <div class="text">公司地址：贵阳市南明区花果园R1区九栋七层719、720号</div>

                    </li>
                    <li>
                        <div style="width:700px;height:550px;border:#ccc solid 1px;font-size:12px;margin-top: 20px" id="map"></div>
                    </li>

                </ul>
                <div class="media_map">
                    <iframe id="media_map_a" width='400' height='500' style="margin:0 auto;" frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='http://f.amap.com/17mdU_0492tsf'></iframe>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="right-qq">
        <a class="pst-rlt" href="tencent://message/?uin=1035578968&Site=&menu=yes">
            <img src="./img/qq-icon-bg.gif" alt="" />
            <div class="text">
                24小时在线解答
            </div>
        </a>
        <a class="pst-rlt online" href="##">
            <img src="./img/right_online.png" alt="" />
            <div class="text">
                热线电话：<br />0851-8511905
            </div>
        </a>
        <a class="pst-rlt online2" href="tencent://message/?uin=1035578968&Site=&menu=yes">
            <img src="./img/right_online.png" alt="" />
            <div class="text">
                24小时在线咨询
            </div>
    </a>
    <div class="weixin pst-rlt">
        微信扫一扫了解更多
        <div class="pic pos-abs">
            <img src="./img/index_bottom_erweima.png" alt="" />
        </div>
    </div>
</div>
<div class="bottom-nav">
    <div class="content">
        <div class="b-nav">
            <div class="name">导航</div>
            <ul>
                <li><a href="index.aspx">主页</a><a href="product.aspx">产品</a></li>
                <li><a href="news.aspx">新闻</a><a href="case.aspx">案例</a></li>
                <li><a href="us.aspx">关于我们</a></li>
            </ul>
        </div>
        <div class="info">
            <ul>
                <li class="logo">
                    <img src="./img/index_blogo.jpg" alt="" /></li>
                <li>
                    <img src="./img/companyname.png" class="info_img" alt="" /></li>
                <li>
                    <img src="./img/index_bm_phone.jpg" alt="" /><span>+86 0851-8511905</span></li>
                <li>
                    <img src="./img/index_bm_mail.jpg" alt="" /><span>1035578968@qq.com</span></li>
                <li class="adress">
                    <img src="./img/index_bm_adress.jpg" alt="" /><span>中国贵州贵阳国家高新技术产业开发区</span></li>
            </ul>
        </div>
        <div class="weixin">
            <img src="./img/index_bottom_erweima.png" alt="" />
            <div class="text">官方微信号</div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="content">© 2015 LightMagic,Inc 黔ICP备16011091号</div>
</div>
        

        <div id="player">
	<div class="cover"></div>
	<div class="ctrl">
		<div class="tag">
			<strong>Title</strong>
			<span class="artist">Artist</span>
			<span class="album">Album</span>
		</div>
		<div class="control">
			<div class="left">
				<div class="rewind icon"></div>
				<div class="playback icon"></div>
				<div class="fastforward icon"></div>
			</div>
			<div class="volume right">
				<div class="mute icon left"></div>
				<div class="slider left">
					<div class="pace"></div>
				</div>
			</div>
		</div>
		<div class="progress">
			<div class="slider">
				<div class="loaded"></div>
				<div class="pace"></div>
			</div>
			<div class="timer left">0:00</div>
			<div class="right">
				<div class="repeat icon"></div>
				<div class="shuffle icon"></div>
			</div>
		</div>
	</div>
</div>


     <script src="Script/js/jquery-ui-1.8.17.custom.min.js"></script>
    <script src="Script/js/script.js"></script>

</body>
<script type="text/javascript">
    //创建和初始化地图函数：
    function initMap(){
        createMap();//创建地图
        setMapEvent();//设置地图事件
        addMapControl();//向地图添加控件
        addMapOverlay();//向地图添加覆盖物
    }
    function createMap(){
        map = new BMap.Map("map");
        map.centerAndZoom(new BMap.Point(106.677542,26.565095),16);
    }
    function setMapEvent(){
        map.enableScrollWheelZoom();
        map.enableKeyboard();
        map.enableDragging();
        map.enableDoubleClickZoom()
    }
    function addClickHandler(target,window){
        target.addEventListener("click",function(){
            target.openInfoWindow(window);
        });
    }
    function addMapOverlay(){
        var markers = [
            {content:"贵阳市南明区花果园R1区九栋七层",title:"贵阳科创科技发展有限公司",imageOffset: {width:0,height:3},position:{lat:26.565224,lng:106.677865}}
        ];
        for(var index = 0; index < markers.length; index++ ){
            var point = new BMap.Point(markers[index].position.lng,markers[index].position.lat);
            var marker = new BMap.Marker(point,{icon:new BMap.Icon("http://api.map.baidu.com/lbsapi/createmap/images/icon.png",new BMap.Size(20,25),{
                imageOffset: new BMap.Size(markers[index].imageOffset.width,markers[index].imageOffset.height)
            })});
            var label = new BMap.Label(markers[index].title,{offset: new BMap.Size(25,5)});
            var opts = {
                width: 200,
                title: markers[index].title,
                enableMessage: false
            };
            var infoWindow = new BMap.InfoWindow(markers[index].content,opts);
            marker.setLabel(label);
            addClickHandler(marker,infoWindow);
            map.addOverlay(marker);
        };
    }
    //向地图添加控件
    function addMapControl(){
        var scaleControl = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
        scaleControl.setUnit(BMAP_UNIT_IMPERIAL);
        map.addControl(scaleControl);
        var navControl = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
        map.addControl(navControl);
        var overviewControl = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:true});
        map.addControl(overviewControl);
    }
    var map;
    initMap();
</script>
</html>

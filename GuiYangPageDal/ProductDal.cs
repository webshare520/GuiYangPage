﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GuiYangPageModel;
using System.Data.SqlClient;
namespace GuiYangPageDal
{
  public  class ProductDal
    {
       DatabaseSql db;
      //构造函数
      public ProductDal()
      {
          db = new DatabaseSql();
          
      }
    
      /// <summary>
      /// 得到一个对象实体
      /// </summary>
      /// <returns></returns>
      //public DataSet GetListPro()
      //{
      //    StringBuilder sb = new StringBuilder();
      //    sb.Append("select FeaText from Product where ProductId=4");
       
      //    Product proModel = new Product();
      //    DataSet ds = db.GetDataSet(sb.ToString());
      //    proModel.Feature = ds.Tables[0].Rows[0][2].ToString();
      //    proModel.FeaText = ds.Tables[0].Rows[0][3].ToString();
       
      //    return ds;

      //}
      /// <summary>
      /// 读取产品信息
      /// </summary>
      /// <returns></returns>
      public SqlDataReader GetProInfo1()
      {
          string sql = "select   FeaText from Product where ProductId=1 ";
         SqlDataReader dr= db.GetDataReader(sql);
         return dr;
      }
      public SqlDataReader GetProInfo2()
      {
          string sql = "select   FeaText from Product where ProductId=2 ";
          SqlDataReader dr = db.GetDataReader(sql);
          return dr;
      }
      public SqlDataReader GetProInfo3()
      {
          string sql = "select   FeaText from Product where ProductId=3 ";
          SqlDataReader dr = db.GetDataReader(sql);
          return dr;
      }
      public SqlDataReader GetProInfo4()
      {
          string sql = "select   FeaText from Product where ProductId=4 ";
          SqlDataReader dr = db.GetDataReader(sql);
          return dr;
      }
      /// <summary>
      /// 读取产品应用信息
      /// </summary>
      /// <returns></returns>
      public SqlDataReader GetProYg1()
      {
          string sql = "select  ProContent from Product where ProductId=1 ";
          SqlDataReader dr = db.GetDataReader(sql);
          return dr;
      }
      public SqlDataReader GetProYg2()
      {
          string sql = "select  ProContent from Product where ProductId=2 ";
          SqlDataReader dr = db.GetDataReader(sql);
          return dr;
      }
      public SqlDataReader GetProYg3()
      {
          string sql = "select  ProContent from Product where ProductId=3 ";
          SqlDataReader dr = db.GetDataReader(sql);
          return dr;
      }
    }
}

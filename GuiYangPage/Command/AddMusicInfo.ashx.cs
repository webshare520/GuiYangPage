﻿using GuiYangPageBll;
using GuiYangPageModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GuiYangPage.Command
{
    /// <summary>
    /// AddMusicInfo 的摘要说明
    /// </summary>
    public class AddMusicInfo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
           
            string name = context.Request["musicName"].ToString();
           
            int count = context.Request.Files.Count;
            if (count > 0)
            {
                HttpPostedFile file = context.Request.Files[0];
                //获取文件扩展名
                string fileExtName = Path.GetExtension(file.FileName);//		file.ContentType	"audio/mp3"	string
                //判断文件类型  video/x-mpeg  video/mpeg4//	video/avi
                if (file.ContentType == "audio/mp3" || file.ContentType == "audio/aiff" || file.ContentType == "audio/basic" || file.ContentType == "video/mpeg")
                {
                    Random r = new Random();
                    string fileName=DateTime.Now.ToString("yyyy-MM--dd")+r.Next(100,1000)+fileExtName;
                    string filePath=context.Server.MapPath("/mp3");
                    filePath = filePath + "/" + fileName;
                    file.SaveAs(filePath);
                    Music music = new Music();
                    music.Name = name;
                    music.Date = DateTime.Now;
                    music.Src = "/mp3"+"/"+fileName;
                    MusicBll bll = new MusicBll();
                     int i=  bll.AddMusicInfo(music);
                     if (i > 0)
                     {
                         context.Response.Write("恭喜：歌曲上传成功！");
                     }
                     else {
                         context.Response.Write("提示：所传歌曲失败！");
                     }
                }
                else
                {
                    context.Response.Write("提示：请选择正确的歌曲文件类型！");
                }
            }
            else
            {
                context.Response.Write("提示：请选择上传文件");
            }
           
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
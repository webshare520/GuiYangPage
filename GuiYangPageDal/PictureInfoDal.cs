﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GuiYangPageModel;
using System.Data;
using System.Data.SqlClient;
namespace GuiYangPageDal
{
    public class PictureInfoDal
    {
        DatabaseSql db;
        public PictureInfoDal()
        {
            db = new DatabaseSql();
        }
          public PictureInfo PictureModel( int id)
      {
          string sql="select * from  PictureInfo where id=@id";
          SqlParameter [] pms={
                              new SqlParameter("@id",SqlDbType.Int){Value=id}
                              };
         DataTable dt=db.ExecuteDataTable (sql,CommandType.Text,pms);
        PictureInfo picture=null;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            picture = new PictureInfo();
            picture.Title = dt.Rows[i]["Title"].ToString();
            picture.Pricture = dt.Rows[i]["Picture"] as string;

        }
         
         
          return picture;
      }
          #region MyRegion
          
          
          /// <summary>
        /// 增加一条
        /// </summary>
        /// <param name="news"></param>
        //public void AddPicture(PictureInfo p)
        //{
        //    string sql = "select top 1 * from PictureInfo";
        //    DataSet ds = db.GetDataSet(sql);
        //    DataRow dr = ds.Tables[0].NewRow();
        //    dr["Picture"] = p.Pricture;
        //    dr["Price"] = p.Price;
        //    dr["Title"] = p.Title;
        //    ds.Tables[0].Rows.Add(dr);
        //    db.UpdateDataset(sql, ds);
        //}
        ///// <summary>
        ///// 增加一条产品
        ///// </summary>
        ///// <param name="p"></param>
        ///// <returns></returns>
        //public int AddProductInfo(PictureInfo p)
        //{
        //    string sql = "insert into PictureInfo(Picture,Price,Title) values(@Picture,@Price,@Title)";
        //    SqlParameter[] pms = {
        //                           new SqlParameter("@Picture",SqlDbType.NVarChar, 50),
        //                           new SqlParameter("@Price",SqlDbType.NChar, 10),
        //                           new SqlParameter("@Title",SqlDbType.NVarChar, 30),
        //                       };
        //    pms[0].Value = p.Pricture;
        //    pms[1].Value = p.Price;
        //    pms[2].Value = p.Title;
        //    int exceSql = db.ExecuteNonQuery(sql, CommandType.Text, pms);
        //    if (exceSql == null)
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return Convert.ToInt32(exceSql);
        //    }

          //}
       #endregion
          /// <summary>
        /// 获取条数
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM PictureInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = db.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from PictureInfo T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return db.Query(strSql.ToString());
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public PictureInfo DataRowToModel(DataRow row)
        {
            //定义一个model对象
            PictureInfo model = new PictureInfo();
            if (row != null)
            {
                if (row["ID"] != null && row["ID"].ToString() != "")
                {
                    model.Id = int.Parse(row["ID"].ToString());
                }
                if (row["Picture"] != null)
                {
                    model.Pricture = row["Picture"].ToString();
                }
                if (row["Price"] != null)
                {
                    model.Price = row["Price"].ToString();
                }
                if (row["Title"] != null && row["Title"].ToString() != "")
                {
                    model.Title = row["Title"].ToString();
                }
            }
            return model;
        }

        #region ==================关于产品添加的方法=============================


        /// <summary>
        ///   定义一个方法。得到所有新闻的信息
        /// </summary>
        /// <returns></returns>
        //public DataSet getPricture()
        //{
        //    string sql = "select * from PictureInfo";
        //    DataSet ds = db.GetDataSet(sql);

        //    return ds;
        //}

        //分页得到新闻
        public DataSet getPrictureByPage(int pageNo, int pageSize, string title)
        {
            string sql = "select top " + pageSize + "  * from PictureInfo where ID not in(select top  " + (pageNo - 1) * pageSize + " ID from PictureInfo where title like '%" + title + "%' order by Date) and title like '%" + title + "%' order by Date ";
            DataSet ds = db.GetDataSet(sql);
            return ds;
        }
        //得到新闻的数量
        public int getPrictureCount(string title)
        {
            string sql = "select * from PictureInfo where Title like '%" + title + "%'";
            DataSet ds = db.GetDataSet(sql);
            return ds.Tables[0].Rows.Count;
        }
        //根据资讯ID删除资讯内容
        public int deletePricture(string[] ids)
        {
            string sql = "";
            for (int i = 0; i < ids.Length; i++)
            {
                if (i == 0)
                    sql = "delete PictureInfo where ID= " + ids[i];
                else
                    sql = sql + " or ID=" + ids[i];
            }
            int newscount = db.ExecSql(sql);
            return newscount;

        }
        //添加资讯
        public void addPricture(PictureInfo p)
        {
            string sql = "select top 1 * from PictureInfo order by ID";
            DataSet ds = db.GetDataSet(sql);
            //创建新的一行
            DataRow dr = ds.Tables[0].NewRow();
            //ID, Picture, Price, Title, Date
            dr["Title"] = p.Title;         
            dr["Price"] = p.Price;
            dr["Picture"] = p.Pricture;
            dr["Date"] = p.Date;
            ds.Tables[0].Rows.Add(dr);
            db.UpdateDataset(sql, ds);


        }
        //更新资讯
        //public int updatePricture(PictureInfo p)
        //{
        //    //ID, Picture, Price, Title, Date
        //    string sqll = "update PictureInfo set Picture='" + p.Pricture + "',Price='" + p.Price + "',title='" + p.Title + "' where Id='" + p.Id + "'";
        //    int i = db.ExecSql(sqll);
        //    return i;
        //}

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Common;
namespace GuiYangPage.Command
{
    /// <summary>
    /// ValidateCodeImageHandler 的摘要说明
    /// </summary>
    public class ValidateCodeImageHandler : IHttpHandler,IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //创建验证码类的对象
            ValidateCode code = new ValidateCode();
            string str = code.CreateValidateCode(5);
            //将生成的验证码保存在session变量里面
            context.Session["Vcode"] = str;
            //创建验证码生成的图片
            code.CreateValidateGraphic(str, context);
           
           // context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
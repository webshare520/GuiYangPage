﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Common;
namespace GuiYangPage
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }
        // 1s:1w个错误写入的请求。
        //整个网站出息异常的时候，都会执行此方法
        protected void Application_Error(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Common.logHelper.logBasePath))
            {
                Common.logHelper.logBasePath = Request.MapPath("/log/");

            }
            //把页面出现的异常放到对象集合中
            logHelper.exceptionInfoQueue.Enqueue(Server.GetLastError().ToString());
            Response.Redirect("index.aspx");
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GuiYangPageDal;
using System.Data.SqlClient;
using GuiYangPageModel;
namespace GuiYangPageBll
{
  public  class CpyAdminBll
    {
      CpyAdminDal dal = new CpyAdminDal();
      public SqlDataReader GetProAdmin()
      {
          return dal.GetProAdmin();
      }
      public bool GetLogin(string name, string pwd)
      {
          return dal.GetLogin(name, pwd) > 0;
      }    
    }
}

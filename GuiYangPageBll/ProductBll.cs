﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GuiYangPageDal;
using System.Data;
using System.Data.SqlClient;
using GuiYangPageModel;
namespace GuiYangPageBll
{
    public class ProductBll
    {
        ProductDal dal = new ProductDal();
        //public DataSet GetListPro()
        //{
        //    return dal.GetListPro();
        //}
        public SqlDataReader GetProInfo1()
        {
            return dal.GetProInfo1();
        }
        public SqlDataReader GetProInfo2()
        {
            return dal.GetProInfo2();
        }
        public SqlDataReader GetProInfo3()
        {
            return dal.GetProInfo3();
        }
        public SqlDataReader GetProInfo4()
        {
            return dal.GetProInfo4();
        }
        /// <summary>
        /// 读取产品应用信息
        /// </summary>
        /// <returns></returns>
        public SqlDataReader GetProYg1()
        {
            return dal.GetProYg1();
        }
        public SqlDataReader GetProYg2()
        {
            return dal.GetProYg2();
        }
        public SqlDataReader GetProYg3()
        {
            return dal.GetProYg3();
        }

       
    }
}

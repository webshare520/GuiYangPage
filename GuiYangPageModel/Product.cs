﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuiYangPageModel
{
    #region  Model
    public class Product
    {
        private int _productId;
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }
        private string _introduction;

        public string Introduction
        {
            get { return _introduction; }
            set { _introduction = value; }
        }
        private string _feature;

        public string Feature
        {
            get { return _feature; }
            set { _feature = value; }
        }
        private string _FeaText;

        public string FeaText
        {
            get { return _FeaText; }
            set { _FeaText = value; }
        }
        private string _proName;

        public string ProName
        {
            get { return _proName; }
            set { _proName = value; }
        }


        private string _proContent;

        public string ProContent
        {
            get { return _proContent; }
            set { _proContent = value; }
        }
    }
    #endregion
}

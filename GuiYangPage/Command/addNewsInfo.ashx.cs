﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GuiYangPageModel;
using GuiYangPageBll;
using System.IO;
namespace GuiYangPage.Command
{
    /// <summary>
    /// addNewsInfo 的摘要说明
    /// </summary>
    public class addNewsInfo : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {

            context.Response.ContentType = "text/plain";

                string title, content;
                News ns = new News();
                title = context.Request.Form["txtDesn"].ToString();
                content = context.Request["txtBody"].ToString();
                string fileDir;//图片文件的上传的路径
                string filename;//文件名
                int cou = context.Request.Files.Count;

            if (cou > 0)//判断用户是否上传了文件
            {
                HttpPostedFile file = context.Request.Files[0];
                //得到档的扩展名(包含.)
                string extName = Path.GetExtension(file.FileName);
                if (file.ContentType == "image/jpeg" || file.ContentType == "image/jpg" || file.ContentType == "image/png" || file.ContentType == "image/gif")
                {
                    Random ran = new Random();
                    // /2016-12-06926.jpg
                    filename = DateTime.Now.ToString("yyyy-MM-dd") + ran.Next(100, 1000) + extName;//给文件取名
                    fileDir = context.Server.MapPath("/image");//用户文件夹的物理路径（ 绝对路径） 
                    fileDir = fileDir + "/" + filename;//档的完整名称
                    file.SaveAs(fileDir);//保存档到服务器
                    ns.Title = title;
                    ns.Content = content;
                    ns.Picture = "/image" + "/" + filename;
                    NewsBll bll = new NewsBll();
                    bll.addNews(ns);
                    context.Response.Write("信息上传成功！");
                }
                else
                {
                    context.Response.Write("信息上传失败！");
                }
            }
            else
            {
                context.Response.Write("请选择要上传的图片！");
            }
           // context.Response.Write("Hello World");

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GuiYangPageBll;
using GuiYangPageModel;
using System.Data;
namespace GuiYangPage.Command
{
    /// <summary>
    /// GetPictureinfo 的摘要说明
    /// </summary>
    public class GetPictureinfo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            PictureInfo p = new PictureInfo();
            PictureInfoBll bll = new PictureInfoBll();
            string type = context.Request["type"].ToString();
            #region  //资讯内容
            if (type == "show")
            {
                int pageNo = int.Parse(context.Request["page"].ToString());
                int pageSize = int.Parse(context.Request["rows"].ToString());
                string title = "";
                if (context.Request["title"] != null)
                    title = context.Request["title"].ToString().Trim();
                int count = bll.getPrictureCount(title);
                DataSet ds = bll.getPrictureByPage(pageNo, pageSize, title);
                string json;
                json = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0], new Newtonsoft.Json.Converters.DataTableConverter());
                json = "{\"total\":" + count.ToString() + ",\"rows\":" + json + "}";
                context.Response.Write(json);
            }
            #endregion

            #region //删除资讯
            else if (type == "del")
            {
                string ids = context.Request["id"].ToString();
                string[] id = ids.Split(',');
                int count = bll.deletePricture(id);
               
                context.Response.Write(count);
            }
            #endregion
            #region // 编辑资讯
            //else if (type == "edit")
            //{
            //    string title, content, picture;
                
            //    //实例化并创造一个对象ns
            //    News ns = new News();
            //    //从客户端获取信息标题赋值于title
            //    title = context.Request["title"].ToString();
            //    content = context.Request["content"].ToString();
            //    picture = context.Request["picture"].ToString();
            //    ns.Title = title;
            //    ns.Content = content;
            //    ns.Picture = picture;
            //    bll.updatePricture(p);
            //    context.Response.Write("更新成功！");
            //}
            #endregion


          //  context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
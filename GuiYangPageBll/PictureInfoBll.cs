﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GuiYangPageDal;
using GuiYangPageModel;
namespace GuiYangPageBll
{
  public  class PictureInfoBll
    {



      PictureInfoDal dal = new PictureInfoDal();
      //public void AddPicture(PictureInfo p)
      //{
      //    dal.AddPicture(p);
      //}
      //public int AddProductInfo(PictureInfo p)
      //{
      //    return dal.AddProductInfo(p);
      //}
      /// <summary>
      /// 获取记录总数
      /// </summary>
      public int GetRecordCount(string strWhere)
      {
          return dal.GetRecordCount(strWhere);
      }
      /// <summary>
      /// 分页获取数据列表
      /// </summary>
      public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
      {
          return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
      }
      /// <summary>
      /// 获得数据列表
      /// </summary>
      public List<PictureInfo> DataTableToList(DataTable dt)
      {
          List<PictureInfo> modelList = new List<PictureInfo>();
          int rowsCount = dt.Rows.Count;
          if (rowsCount > 0)
          {
              PictureInfo model;
              for (int n = 0; n < rowsCount; n++)
              {
                  model = dal.DataRowToModel(dt.Rows[n]);
                  if (model != null)
                  {
                      modelList.Add(model);
                  }
              }
          }
          return modelList;
      }

      //public DataSet getPricture()
      //{
      //    return dal.getPricture();
      //}
      public DataSet getPrictureByPage(int pageNo, int pageSize, string title)
      {
          return dal.getPrictureByPage(pageNo, pageSize, title);
      }
      //得到新闻的数量
      public int getPrictureCount(string title)
      {
          return dal.getPrictureCount(title);
      }
      //根据资讯ID删除资讯内容
      public int deletePricture(string[] ids)
      {
          return dal.deletePricture(ids);
      }
      //添加资讯
      public void addPricture(PictureInfo p)
      {
          dal.addPricture(p);
      }
      //public int updatePricture(PictureInfo p)
      //{
      //    return dal.updatePricture(p);
      //}
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="GuiYangPage.index" EnableViewState="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<!DOCTYPE html>
<head runat="server">
    <title>贵阳科创科技发展有限公司-官网</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="贵阳科创科技发展有限公司于2007年2月正式注册成立，是专业为科技馆、博物馆、纪念馆、规划馆、游乐园，展示厅等提供创意策划、设计及项目建设的民营高新技术企业 、软件企业。" />
    <meta name="keywords" content="球幕影院,360度环幕影院,弧幕影院,互动多媒体,镜子迷宫,时空隧道,动感球幕影院,模拟飞行器,时光隧道,八卦迷宫,豪华镜子迷宫
" />
    <meta charset="UTF-8" />
    <link href="Script/css/main.css" rel="stylesheet" />
    <link href="Script/css/index.css" rel="stylesheet" />
    <link href="Script/css/jquery.slideBox.css" rel="stylesheet" />
    <link href="Script/css/style_m.css" rel="stylesheet" />
    <link href="Script/css/media.css" rel="stylesheet" />
    <script src="Script/js/jquery.js"></script>
    <script src="Script/js/main.js"></script>
    <script src="Script/js/jquery.rotate.min.js"></script>
    <script src="Script/js/jquery.animated.js"></script>
    <script src="Script/js/slider.js"></script>
</head>

<body>
    <form id="form1" runat="server">
        <div class="nav">
            <div class="content">
                <div class="logo">
                    <img src="./img/logo.png" alt="" />
                </div>
                <div class="nav-name">
                    <ul>
                        <li><a href="index.aspx" class="active">主页</a></li>
                        <li><a href="product.aspx">产品</a></li>
                        <li><a href="case.aspx">案例</a></li>
                        <li><a href="news.aspx">新闻</a></li>
                        <li><a href="us.aspx">关于我们</a></li>
                    </ul>
                </div>
                <div class="both"></div>
            </div>
        </div>
        <div id="banner_tabs" class="flexslider">
            <ul class="slides">
                <li>
                    <a title="" target="_blank" href="pdetail.aspx">
                        <img src="./img/bnr_1.jpg" />
                    </a>
                </li>
                <li>
                    <a title="" target="_blank" href="pdetail.aspx">
                        <img src="./img/bnr_2.jpg" />
                    </a>
                </li>
                <li>
                    <a title="" target="_blank" href="pdetail.aspx">
                        <img src="./img/bnr_3.jpg" />
                    </a>
                </li>
                <li>
                    <a title="" target="_blank" href="pdetail.aspx">
                        <img src="./img/bnr_4.jpg" />
                    </a>
                </li>
                <li>
                    <a title="" target="_blank" href="pdetail.aspx">
                        <img src="./img/bnr_5.jpg" />
                    </a>
                </li>

            </ul>

            <ul class="flex-direction-nav">
                <li><a class="flex-prev" href="javascript:;">Prev</a></li>
                <li><a class="flex-next" href="javascript:;">Next</a></li>
            </ul>
            <div class="both"></div>
            <ol id="bannerCtrl" class="flex-control-nav flex-control-paging">
                <li class="active"><a>1</a></li>
                <li><a>2</a></li>
                <li><a>3</a></li>
                <li><a>4</a></li>
                <li><a>5</a></li>

            </ol>
            <div class="both"></div>
        </div>
        <div class="product content">
            <div class="title ">
                <ul>
                    <li class="d1"></li>
                    <li class="d2">产 品</li>
                </ul>
            </div>
            <div class="info">
                <div class="left">
                    <img src="./img/index_product_pic.jpg" alt="" />
                </div>
                <div class="right">
                    <asp:Label ID="labInduct" runat="server" Text="Label"></asp:Label>

                </div>
            </div>
            <div class="point">
                <ul>
                    <li class="list01">
                        <a href="pdetail.aspx" target="_blank">
                            <div class="pic">
                                <img src="./img/index_product_point1.png" alt="" />
                            </div>
                            <div class="name">
                                技术先进                         
                            </div>
                            <div class="text  ani">
                                <asp:Label ID="labFatText1" runat="server" Text="Label"></asp:Label>
                            </div>
                        </a>
                    </li>
                    <li class="list02">
                        <a href="pdetail.aspx" target="_blank">
                            <div class="pic">
                                <img src="./img/index_product_point2.png" alt="" />
                            </div>
                            <div class="name">
                                操作简单                         
                            </div>
                            <div class="text  ani">
                                <asp:Label ID="labFatText2" runat="server" Text="Label"></asp:Label>

                            </div>
                        </a>
                    </li>
                    <li class="list03">
                        <a href="pdetail.aspx" target="_blank">
                            <div class="pic">
                                <img src="./img/index_product_point3.png" alt="" />
                            </div>
                            <div class="name">
                                一体集成
                            </div>
                            <div class="text  ani">
                                <asp:Label ID="labFatText3" runat="server" Text="Label"></asp:Label>

                            </div>
                        </a>
                    </li>
                    <li class="list04">
                        <a href="pdetail.aspx" target="_blank">
                            <div class="pic">
                                <img src="./img/index_product_point4.png" alt="" />
                            </div>
                            <div class="name">
                                播放灵活
                            </div>
                            <div class="text  ani">
                                <asp:Label ID="labFatText4" runat="server" Text="Label"></asp:Label>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="case content">
            <div class="title ">
                <ul>
                    <li class="d1"></li>
                    <li class="d2">应用案例</li>
                </ul>
            </div>
            <div class="list">
                <ul>
                    <li class="list1">
                        <a href="case.aspx" target="_blank">
                            <div class="pic">
                                <img src="./img/index_case_list1.jpg" alt="" /></div>
                            <div class="text">
                                <div class="name">
                                    骨架式球幕影院
                                </div>
                                <div class="info">
                                    <asp:Label ID="labeMovie" runat="server" Text="Label"></asp:Label>

                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="list2">
                        <a href="case.aspx" target="_blank">
                            <div class="pic transition500">
                                <img src="./img/index_case_list2.jpg" alt="" /></div>
                            <div class="text">
                                <div class="name">
                                    虚拟过山车
                                </div>
                                <div class="info">
                                    <asp:Label ID="labChe" runat="server" Text="Label"></asp:Label>

                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="list3">
                        <a href="case.aspx" target="_blank">
                            <div class="pic transition500">
                                <img src="./img/index_case_list3.jpg" alt="" /></div>
                            <div class="text">
                                <div class="name">
                                    触摸互动翻书
                                </div>
                                <div class="info">
                                    <asp:Label ID="labBook" runat="server" Text="Label"></asp:Label>

                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="case content">
            <div class="title ">
                <ul>
                    <li class="d1"></li>
                    <li class="d2">公司资质</li>
                </ul>
            </div>
            <div class="list">
                <ul>
                    <li class="list1">
                        <a href="us.aspx" target="_blank">
                            <div class="pic">
                                <img src="./img/gfdg.jpg" alt="" /></div>
                            <div class="text">
                                <div class="name">
                                    高新技术企业
                                </div>
                                <div class="info">
                                    <%--<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>--%>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="list2">
                        <a href="us.aspx" target="_blank">
                            <div class="pic transition500">
                                <img src="./img/hshshsg.jpg" alt="" /></div>
                            <div class="text">
                                <div class="name">
                                    贵阳省技术型企业备案证书
                                </div>
                                <div class="info">
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="list3">
                        <a href="us.aspx" target="_blank">
                            <div class="pic transition500">
                                <img src="./img/ssds.jpg" alt="" /></div>
                            <div class="text">
                                <div class="name">
                                    软件企业认证证书
                                </div>
                                <div class="info">
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="news ">
            <div class="content">
                <div class="title">
                    <ul>
                        <li class="d1"></li>
                        <li class="d2">新 闻</li>
                    </ul>
                </div>
                <ul>
                    <li class="row">
                        <div class="left">
                            <img src="./img/index_news1.jpg" alt="" />
                        </div>
                        <div class="right">
                            <a href="ndetail.aspx" target="_blank">
                                <div class="name">
                                    <p>你应该知道自己的体重</p>
                                </div>
                                <div class="date">2016-11-22</div>
                                <div class="text">

                                    <p>
                                        你知道你在水星上的体重是多少呢？哗，一站上
去行星秤上，不仅可以秤出观众在地球上的体重，还可以
在多媒体上显示观众在该行星上的体重，虽然不能在太阳系中
玩“星际“漫游，但也可以小小地满足一下。这些行星包括我们
的地球母亲每时每刻都围绕着太阳转啊转的，但是这些行星上
条件很艰苦，都没有生命的存在。而且由于各行星的成因、
地理构造、环境、体积、质量、引力及气候等不同，
因而相同的物体在不同的行星上就
有不同的重量。
                                    </p>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="row">
                        <div class="left">
                            <img src="./img/index_news2.jpg" alt="" />
                        </div>
                        <div class="right">
                            <a href="ndetail.aspx" target="_blank">
                                <div class="name">
                                    <p>贵阳科创科技发展有限公司</p>
                                </div>
                                <div class="date">
                                    2016-11-23
                                </div>
                                <div class="text">

                                    <p>贵阳科创科技发展有限公司成立于2007年2月，本公司是一家立足于视觉软件及科普互动产品、大型游乐产品研发为主的专业性高科技企业，公司秉诚“诚信经营为本、创新促进发展“为理念，拥有独立自主知识产权产品多项，其研发的产品有：动感地面互动系统，手影翻书系统，雾帘成像系统，内外球显系统。贵阳科创科技发展有限公司成立于2007年2月，本公司是一家立足于视觉软件及科普互动产品、大型游乐产品研发为主的专业性高统。</p>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="row">
                        <div class="left">
                            <img src="./img/index_news3.jpg" alt="" />
                        </div>
                        <div class="right">
                            <a href="ndetail.aspx" target="_blank">
                                <div class="name">
                                    <p>二代360度</p>
                                </div>
                                <div class="date">2016-11-24</div>
                                <div class="text">

                                    <p>立体动感球幕影院由贵阳科创科技发展有限公司自主独立研发，生产，拥有独立、自主知识产权，在室内外均可放映，搭建快速，运输方便，放映效果震憾力强，视角冲击力更加辉煌 ，在技术上属于国内首家首创的龙头产品，处于领先地位。</p>
                                </div>
                            </a>
                        </div>
                    </li>
                </ul>
                <div>
                </div>
            </div>
        </div>
        <div class="right-qq">
            <a class="pst-rlt" href="tencent://message/?uin=1035578968&Site=&menu=yes">
                <img src="./img/qq-icon-bg.gif" alt="" />
                <div class="text">
                    24小时在线解答
                </div>
            </a>
            <a class="pst-rlt online" href="##">
                <img src="./img/right_online.png" alt="" />
                <div class="text">
                    热线电话：<br />
                    0851-8511905
                </div>
            </a>
            <a class="pst-rlt online2" href="tencent://message/?uin=1035578968&Site=&menu=yes">
                <img src="./img/right_online.png" alt="" />
                <div class="text">
                    24小时在线咨询
                </div>
            </a>
            <div class="weixin pst-rlt">
                微信扫一扫了解更多
            <div class="pic pos-abs">
                <img src="./img/index_bottom_erweima.png" alt="" />
            </div>
            </div>
        </div>
        <div class="bottom-nav">
            <div class="content">
                <div class="b-nav">
                    <div class="name">导航</div>
                    <ul>
                        <li><a href="index.aspx">主页</a><a href="product.aspx">产品</a></li>
                        <li><a href="news.aspx">新闻</a><a href="case.aspx">案例</a></li>
                        <li><a href="us.aspx">关于我们</a></li>
                    </ul>
                </div>
                <div class="info">
                    <ul>
                        <li class="logo">
                            <img src="./img/index_blogo.jpg" alt="" /></li>
                        <li>
                            <img src="./img/companyname.png" class="info_img" alt="" /></li>
                        <li>
                            <img src="./img/index_bm_phone.jpg" alt="" /><span>+86 0851-8511905</span></li>
                        <li>
                            <img src="./img/index_bm_mail.jpg" alt="" /><span>1035578968@qq.com</span></li>
                        <li class="adress">
                            <img src="./img/index_bm_adress.jpg" alt="" /><span>中国贵州贵阳国家高新技术产业开发区</span></li>
                    </ul>
                </div>
                <div class="weixin">
                    <img src="./img/index_bottom_erweima.png" alt="" />
                    <div class="text">官方微信号</div>
                </div>
            </div>
        </div>


        <div id="player">
            <div class="cover"></div>
            <div class="ctrl">
                <div class="tag">
                    <strong>Title</strong>
                    <span class="artist">Artist</span>
                    <span class="album">Album</span>
                </div>
                <div class="control">
                    <div class="left">
                        <div class="rewind icon"></div>
                        <div class="playback icon"></div>
                        <div class="fastforward icon"></div>
                    </div>
                    <div class="volume right">
                        <div class="mute icon left"></div>
                        <div class="slider left">
                            <div class="pace"></div>
                        </div>
                    </div>
                </div>
                <div class="progress">
                    <div class="slider">
                        <div class="loaded"></div>
                        <div class="pace"></div>
                    </div>
                    <div class="timer left">0:00</div>
                    <div class="right">
                        <div class="repeat icon"></div>
                        <div class="shuffle icon"></div>
                    </div>
                </div>
            </div>
        </div>



        <div class="footer">
            <div class="content">© 2015 LightMagic,Inc 黔ICP备16011091号</div>
        </div>
        <script type="text/javascript">
            //轮播图
            $(function () {
                var bannerSlider = new Slider($('#banner_tabs'), {
                    time: 5000,
                    delay: 400,
                    event: 'hover',
                    auto: true,
                    mode: 'fade',
                    controller: $('#bannerCtrl'),
                    activeControllerCls: 'active'
                });
                $('#banner_tabs .flex-prev').click(function () {
                    bannerSlider.prev()
                });
                $('#banner_tabs .flex-next').click(function () {
                    bannerSlider.next()
                });
            })
        </script>
        <script>
            //图片缩放效果

            $(document).ready(function () {
                $(function () {
                    $w = $('.case  ul li a img').width();
                    $h = $('.case  ul li a img').height();
                    $w2 = $w + 20;
                    $h2 = $h + 20;
                    $('.case  ul li a img').hover(function () {
                        $(this).stop().animate({
                            width: $w2,
                            height: $h2
                        }, 500);
                    }, function () {
                        $(this).stop().animate({
                            height: $h,
                            width: $w,
                            left: "0px",
                            top: "0px"
                        }, 500);
                    });
                });
            });
        </script>

        <script type="text/javascript">
            //图片旋转
            $(document).ready(function () {
                $(".point ul li .pic img").rotate({

                    bind: {
                        mouseover: function () {

                            $(this).rotate({
                                animateTo: 360
                            });

                        },

                        mouseout: function () {

                            $(this).rotate({
                                animateTo: 0
                            });

                        }

                    }

                });

            });
        </script>
        <script>
            $(function () {
                //滚动到指定位置显示


                $(window).scroll(function () {

                    var se = document.documentElement.clientHeight;

                    if ($('.case .list ul ')[0].getBoundingClientRect().top <= se) {

                        $(".case .list ul li.list1 ").delay(100).animate({
                            opacity: "1"
                        }, 1000);
                        $(".case .list ul li.list2 ").delay(600).animate({
                            opacity: "1"
                        }, 1200);
                        $(".case .list ul li.list3 ").delay(1000).animate({
                            opacity: "1"
                        }, 1400);

                    }
                });

                $(window).scroll();
            });
        </script>
        <%--背景音乐控制--%>
        <script>
            $(window).bind("scroll", function () {
                var top = $(this).scrollTop(); // 当前窗口的滚动距离
                if (top == 0) {
                    $("#player").css('height', '0px');
                }
                if (top > 60) {
                    $("#player").css('height', '122px');
                }
            });
        </script>
    </form>
    <script src="Script/js/jquery-ui-1.8.17.custom.min.js"></script>
    <script src="Script/js/script.js"></script>

</body>

</html>


﻿using GuiYangPageDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GuiYangPageModel;
namespace GuiYangPage
{
    public partial class UploadMp3 : BasePage//System.Web.UI.Page
    {
        public List<Music> listMusic { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            MusicDal dal = new MusicDal();
           listMusic=  dal.GetMusicInfo();
           
        }
    }
}
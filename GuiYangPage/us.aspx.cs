﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GuiYangPageDal;
using System.Data;
using System.Data.SqlClient;
using GuiYangPageBll;
namespace GuiYangPage
{
    public partial class us : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                //读取公司简介
                string sql = "select top 1 description from company ";
                DatabaseSql db = new DatabaseSql();
                SqlDataReader dr = db.GetDataReader(sql);
                string CompanyDesc = "";
                while (dr.Read())
                {
                    CompanyDesc = CompanyDesc + dr["Description"].ToString().Trim();
                }
                labDesc.Text = CompanyDesc;           
            }
           
        }
    }
}
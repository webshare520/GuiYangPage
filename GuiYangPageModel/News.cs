﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuiYangPageModel
{
    #region Model
    public class News
    {
        private int _NewId;

        public int NewId
        {
            get { return _NewId; }
            set { _NewId = value; }
        }
        private string _title;
        
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        private DateTime _date;

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }
        private string _content;

        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }
        private string picture;

        public string Picture
        {
            get { return picture; }
            set { picture = value; }
        }
    }
    #endregion
}

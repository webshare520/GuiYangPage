﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Common
{
    public  class MD5Helper
    {
        public static string GetMd5(string txt)
        {
            //创建MD5对象
            MD5 md5 = MD5.Create();
            byte[] by1 = Encoding.UTF8.GetBytes(txt);
            byte[] by2 = md5.ComputeHash(by1);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < by2.Length; i++)
            {
                sb.Append(by2[i].ToString("x2").ToLower());
                
            }
            return sb.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GuiYangPageModel;
namespace GuiYangPage
{
    public class BasePage:System.Web.UI.Page
    {
        public CpyAdmin LoginAdmin { get; set; }
        //所有页面初始化时候先执行Page_Init
        public virtual void Page_Init(object sender, EventArgs e)
        {
            if (Session["loginadmin"] == null)
            {
                Response.Redirect("GLYLogin.aspx");
            }
            LoginAdmin = Session["loginadmin"] as CpyAdmin;
        }

    }
}
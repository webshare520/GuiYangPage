﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GuiYangPageBll;
using GuiYangPageModel;
using System.IO;
using System.Web.SessionState;
namespace GuiYangPage.Command
{
    /// <summary>
    /// addNews 的摘要说明
    /// </summary>
    public class addNews : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            News ns = new News();
        //    var realtitle =context.Request.Form["desn"];
            string title = context.Request.Form["desn"].ToString();
            string content = context.Request["body"].ToString().Trim();
            string fileDir;//图片文件的上传的路径
            string filename;//文件名
            int cou = context.Request.Files.Count;
            if (cou > 0)//判断用户是否上传了文件
            {
                HttpPostedFile file = context.Request.Files[0];
                //得到档的扩展名(包含.)
                string extName = Path.GetExtension(file.FileName);
                if (file.ContentType == "image/jpeg" || file.ContentType == "image/jpg" || file.ContentType == "image/png" || file.ContentType == "image/gif")
                {
                    Random ran = new Random();
                   // /2016-12-06926.jpg
                    filename = DateTime.Now.ToString("yyyy-MM-dd") + ran.Next(100, 1000) + extName;//给文件取名
                    fileDir = context.Server.MapPath("/image");//用户文件夹的物理路径（绝对路径）
                    fileDir = fileDir + "/" + filename;//档的完整名称
                    file.SaveAs(fileDir);//保存档到服务器
                    ns.Title = title;
                    ns.Content = content;
                    ns.Date = DateTime.Now;
                    ns.Picture = "/image" + "/" + filename; //   "/" + u.Username + "/" + filename;
                    NewsBll bll = new NewsBll();
                    bll.addNews(ns);
                    context.Response.Write("信息上传成功！");
                }
                else
                {
                    context.Response.Write("信息上传失败！");
                }
            }
            else
            {
                context.Response.Write("请选择要上传的图片！");
            }          
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GuiYangPageModel
{
    #region Model
    public class Company
    {
        private int _companyId;

        public int CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }
        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
    }
    #endregion
}
